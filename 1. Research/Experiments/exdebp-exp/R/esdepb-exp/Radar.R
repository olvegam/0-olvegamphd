# Set path to Desktop

# Para MAC
setwd("~/proys/0-olvegamphd/1. Research/Experiments/exdebp-exp/Sample Analysis/R/esdepb-exp/Radar")

# Para Windows
#setwd("D:/proys/0-olvegamphd/1. Research/Experiments/exdebp-exp/Sample Analysis/R/esdepb-exp")

# Instalaci??n de paquetes y librer??as

#Para leer archivos de Excel
#install.packages("tidyverse")
# Leer el paquete para lectura de archivos de Excel. 
library("readxl")

#Para utilizar ggplot y graficar los boxplot con t?tulos y dem?s. 
#install.packages("ggplot2")
library(ggplot2)

##############################################################
###   Leer los datos (cada caso y los de ambos casos)      ###
##############################################################

# Lea los datos para el caso de estudio FMSG.
#datosFMSG <- read_excel("CQ-FMSG-Subjects.xlsx", sheet = 1)
# Lea los datos para el caso de estudio PdV.
datosPdV <- read_excel("ScoreAverages.xlsx", sheet = 4)
# Lea los datos para de ambos casos de estudio.
#datosTwoCS <- read_excel("CQ-TwoCS.xlsx", sheet = 1)

datosPdV

###############################################################
###   Mostrar una porcion aleatoria de cada conjunto de datos.# 
###############################################################

# Show first a random sample of each data set. 
set.seed(1234)
dplyr::sample_n(datosFMSG, 10)

# Summary of the data 
summary(datosFMSG)

dplyr::sample_n(datosPdV, 10)

# Summary of the data 
summary(datosPdV)

dplyr::sample_n(datosTwoCS, 10)

# Summary of the data 
summary(datosTwoCS)

###############################################################
###   Organice los niveles de los datos.                    ### 
###############################################################

#Asigne los niveles a la columna que los tiene (en este caso ArtifactsSet) - datos FMSG
datosFMSG$ArtifactsSet <- ordered(datosFMSG$ArtifactsSet, levels(factor(datosFMSG$ArtifactsSet)))
#show the levels
levels(datosFMSG$ArtifactsSet)

#Asigne los niveles a la columna que los tiene (en este caso ArtifactsSet) - datos PdV
datosPdV$ArtifactsSet <- ordered(datosPdV$ArtifactsSet, levels(factor(datosPdV$ArtifactsSet)))
#show the levels
levels(datosPdV$ArtifactsSet)

#Asigne los niveles a la columna que los tiene (en este caso ArtifactsSet) - datos TwoCS
datosTwoCS$ArtifactsSet <- ordered(datosTwoCS$ArtifactsSet, levels(factor(datosTwoCS$ArtifactsSet)))

#show the levels
levels(datosTwoCS$ArtifactsSet)

###############################################################
###   Muestre los boxplot de cada conjunto de datos.        ### 
###############################################################

#Exportar la imagen a png - prepare el archivo. 
png("BoxPlot-FMSG.png")

#Dibuje el boxplot de FMSG (Diagrama de cajas)
ggplot(datosFMSG, aes(ArtifactsSet,Subjects)) + geom_boxplot() +
 ggtitle ("Caso de estudio solicitud de Cr?dito FMSG") +
 theme (plot.title = element_text(vjust=2, hjust=0.5, lineheight=1.5))

#Exportar la imagen a png - genere el archivo. 
dev.off()

#Exportar la imagen a png - prepare el archivo. 
png("BoxPlot-PdV.png")

#Dibuje el boxplot de PdV. (Diagrama de cajas)
ggplot(datosPdV, aes(ArtifactsSet,Subjects)) + geom_boxplot() +
 ggtitle ("Caso de estudio punto de venta") +
 theme (plot.title = element_text(vjust=2, hjust=0.5, lineheight=1.5))

#Exportar la imagen a png - genere el archivo. 
dev.off()

#Exportar la imagen a png - prepare el archivo. 
png("BoxPlot-TwoCS.png")

#Dibuje el boxplot de ambos casos de estudio. (Diagrama de cajas)
ggplot(datosTwoCS, aes(ArtifactsSet,Subjects)) + geom_boxplot()+
 ggtitle ("Datos de dos casos de estudio") +
 theme (plot.title = element_text(vjust=2, hjust=0.5, lineheight=1.5))

#Exportar la imagen a png - genere el archivo. 
dev.off()

###############################################################
###   Analisis de Varianza  -AOV                            ### 
###############################################################

# AOV de caso de estudio FMSG
FMSG.aov <- aov(Subjects ~ ArtifactsSet, data = datosFMSG)

# Summary of the analysis
FileSummaryFMSG = summary(FMSG.aov)
# capture.output(DataForFile, file="AOV-FMSG.doc")

# AOV de caso de estudio PdV
PdV.aov <- aov(Subjects ~ ArtifactsSet, data = datosPdV)

# Summary of the analysis
FileSummaryPdV = summary(PdV.aov)
#capture.output(DataForFile, file="AOV-PdV.doc")

# AOV de datos ambos casos de estudio. 
TwoCS.aov <- aov(Subjects ~ ArtifactsSet, data = datosTwoCS)

# Summary of the analysis
FileSummaryTwoCS = summary(TwoCS.aov)

###############################################################
###   File output creation                                  ### 
###############################################################

# Title
cat("Anova summaries output", file = "Aov-Summaries.doc")
# add 2 newlines
cat("\n\n", file = "Aov-Summaries.doc", append = TRUE)
# export anova test FMSG output
cat("Anova Test FMSG\n", file = "Aov-Summaries.doc", append = TRUE)
capture.output(FileSummaryFMSG, file = "Aov-Summaries.doc", append = TRUE)
# add 2 newlines
cat("\n\n", file = "Aov-Summaries.doc", append = TRUE)
# export anova test PdV output
cat("Anova Test PdV\n", file = "Aov-Summaries.doc", append = TRUE)
capture.output(FileSummaryPdV, file = "Aov-Summaries.doc", append = TRUE)
# add 2 newlines
cat("\n\n", file = "Aov-Summaries.doc", append = TRUE)
# export anova test TwoCS output
cat("Anova Test TwoCS\n", file = "Aov-Summaries.doc", append = TRUE)
capture.output(FileSummaryTwoCS, file = "Aov-Summaries.doc", append = TRUE)
