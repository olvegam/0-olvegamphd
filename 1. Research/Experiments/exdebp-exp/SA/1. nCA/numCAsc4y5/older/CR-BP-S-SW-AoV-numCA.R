# Set path to Desktop

# Para MAC
#setwd("~/proys/0-olvegamphd/1. Research/Experiments/exdebp-exp/SA//1. nCA/numCAsc4y5")

# Para Windows Bogotá - portátil
setwd("D:/proys/0-olvegamphd/1. Research/Experiments/exdebp-exp/SA/1. nCA/numCAsc4y5")

# Para Windows Unillanos
#setwd("C:/proys/0-olvegamphd/1. Research/Experiments/exdebp-exp/SA/1. nCA/numCAsc4y5")

# Instalacion de paquetes y librerias

#Para leer archivos de Excel
#install.packages("tidyverse")
# Leer el paquete para lectura de archivos de Excel. 
library("readxl")

#Para utilizar ggplot y graficar los boxplot con t?tulos y dem?s. 
#install.packages("ggplot2")
library(ggplot2)

##############################################################
###   Leer los datos (cada caso y los de ambos casos)      ###
##############################################################

# Lea los datos de cada distribución por grupo de artefactos para ks test. 
datosMSGB <- read_excel("numCAsc4y5.xlsx", sheet = 5)
datosMSGBU <- read_excel("numCAsc4y5.xlsx", sheet = 6)
datosMSGUC <- read_excel("numCAsc4y5.xlsx", sheet = 7)
datosPSB <- read_excel("numCAsc4y5.xlsx", sheet = 8)
datosPSBU <- read_excel("numCAsc4y5.xlsx", sheet = 9)
datosPSUC <- read_excel("numCAsc4y5.xlsx", sheet = 10)
datosBothB <- read_excel("numCAsc4y5.xlsx", sheet = 11)
datosBothBU <- read_excel("numCAsc4y5.xlsx", sheet = 12)
datosBothUC <- read_excel("numCAsc4y5.xlsx", sheet = 13)

# Lea los datos para el caso de estudio FMSG.
datosFMSG <- read_excel("numCAsc4y5.xlsx", sheet = 2)
# Lea los datos para el caso de estudio PdV.
datosPdV <- read_excel("numCAsc4y5.xlsx", sheet = 3)
# Lea los datos para ambos casos de estudio.
datosTwoCS <- read_excel("numCAsc4y5.xlsx", sheet = 1)


###############################################################
###   Mostrar una porcion aleatoria de cada conjunto de datos.# 
###############################################################

# Show first a random sample of each data set. 
set.seed(1234)

# Show some data 
dplyr::sample_n(datosFMSG, 10)

# Show some data 
dplyr::sample_n(datosPdV, 10)

# Show some data 
dplyr::sample_n(datosTwoCS, 10)

###############################################################
###   Organice los niveles de los datos.                    ### 
###############################################################

#Asigne los niveles a la columna que los tiene (en este caso ArtifactsSet) - datos FMSG
datosFMSG$ArtifactsSet <- ordered(datosFMSG$ArtifactsSet, levels(factor(datosFMSG$ArtifactsSet)))
#show the levels
levels(datosFMSG$ArtifactsSet)

#Asigne los niveles a la columna que los tiene (en este caso ArtifactsSet) - datos PdV
datosPdV$ArtifactsSet <- ordered(datosPdV$ArtifactsSet, levels(factor(datosPdV$ArtifactsSet)))
#show the levels
levels(datosPdV$ArtifactsSet)

#Asigne los niveles a la columna que los tiene (en este caso ArtifactsSet) - datos TwoCS
datosTwoCS$ArtifactsSet <- ordered(datosTwoCS$ArtifactsSet, levels(factor(datosTwoCS$ArtifactsSet)))

#show the levels
levels(datosTwoCS$ArtifactsSet)

#Asigne los niveles a la columna que los tiene (en este caso ArtifactsSet) - datos TwoCS
#datosTwoCSB$ArtifactsSet <- ordered(datosTwoCSB$ArtifactsSet, levels(factor(datosTwoCSB$ArtifactsSet)))

#show the levels
#levels(datosTwoCS$ArtifactsSet)


#Asigne los niveles a la columna que los tiene (en este caso ArtifactsSet) - datos TwoCSBandC
#datosTwoCSBandC$ArtifactsSet <- ordered(datosTwoCSBandC$ArtifactsSet, levels(factor(datosTwoCSBandC$ArtifactsSet)))

#show the levels
#levels(datosTwoCSBandC$ArtifactsSet)

###############################################################
###   Muestre los boxplot de cada conjunto de datos.        ### 
###############################################################

#Dibuje el boxplot de FMSG (Diagrama de cajas)
bMSG = ggplot(datosFMSG, aes(ArtifactsSet,numCA)) + geom_boxplot(outlier.shape = 19, position=position_dodge(0.7), width = 0.3 ) +
#  ggtitle ("Case study credit application on FMSG") +
  labs(x = "Artifacts set", y= "Number of correct answers") + 
  theme (plot.title = element_text(vjust=2, hjust=0.5, lineheight=1.5, size = 20))  +
  theme(axis.title.x = element_text(face="bold", vjust=-0.5, size=rel(1.5))) +
  theme(axis.title.y = element_text(face="bold", vjust=1.5, size=rel(1.5))) +
  theme(axis.text.x = element_text(face="bold", vjust=1.5, size=rel(1.5))) +
  theme(axis.text.y = element_text(face="bold", vjust=1.5, size=rel(1.5))) +
  theme(panel.background = element_rect(fill = "white", colour = "black")) +
#  theme(panel.grid.minor.y = element_line(colour = "grey50")) +
  stat_summary(fun.y=mean, geom="point", shape=18,
               size=3, color="red")

#Exportar la imagen a eps - prepare el archivo. 
setEPS()
postscript("rq1-m1-msg.eps", onefile=FALSE, height=6, width=8, pointsize=10)
print(bMSG)
dev.off()

#Exportar la imagen a png - prepare el archivo. 
png(filename = "rq1-m1-msg.png", width = 1800, height = 1200, units = "px", 
    res = 200)

#Exp?rtelo al tipo de archivo preparado
print(bMSG)
dev.off()

#Exportar la imagen a pdf - prepare el archivo
pdf("rq1-m1-msg.pdf")
print(bMSG)
dev.off()

#Dibuje el boxplot de PdV. (Diagrama de cajas)
bPdV=ggplot(datosPdV, aes(ArtifactsSet,numCA)) + geom_boxplot(outlier.shape = 19,width = 0.3 ) +
#  ggtitle ("Case study point of sale") + 
  labs(x = "Artifacts set", y= "Number of correct answers") + 
  theme (plot.title = element_text(vjust=2, hjust=0.5, lineheight=1.5, size = 20))  +
  theme(axis.title.x = element_text(face="bold", vjust=-0.5, size=rel(1.5))) +
  theme(axis.title.y = element_text(face="bold", vjust=1.5, size=rel(1.5))) +
  theme(axis.text.x = element_text(face="bold", vjust=1.5, size=rel(1.5))) +
  theme(axis.text.y = element_text(face="bold", vjust=1.5, size=rel(1.5))) +
  theme(panel.background = element_rect(fill = "white", colour = "black")) +
#  theme(panel.grid.minor.y = element_line(colour = "grey50")) +
  stat_summary(fun.y=mean, geom="point", shape=18,
               size=3, color="red")

#Exportar la imagen a eps - prepare el archivo. 
setEPS()
postscript("rq1-m1-ps.eps", onefile=FALSE, height=8, width=10, pointsize=10)
print(bPdV)
dev.off()

#Exportar la imagen a png - prepare el archivo. 
png(filename = "rq1-m1-ps.png", width = 1800, height = 1200, units = "px", 
    res = 200)
print(bPdV)
dev.off()

#Exportar la imagen a pdf - prepare el archivo
pdf("rq1-m1-ps.pdf")
print(bPdV)
dev.off()

#Dibuje el boxplot de ambos casos de estudio. (Diagrama de cajas)
bTwoCS = ggplot(datosTwoCS, aes(ArtifactsSet,numCA)) + 
  geom_boxplot(outlier.shape = 19, width = 0.3 )  +
# ggtitle ("Two case studies data") +
  labs(x = "Artifacts set", y= "Number of correct answers") + 
  theme (plot.title = element_text(vjust=2, hjust=0.5, lineheight=1.5, size = 20))  +
  theme(axis.title.x = element_text(face="bold", vjust=-0.5, size=rel(1.5))) +
  theme(axis.title.y = element_text(face="bold", vjust=1.5, size=rel(1.5))) +
  theme(axis.text.x = element_text(face="bold", vjust=1.5, size=rel(1.5))) +
  theme(axis.text.y = element_text(face="bold", vjust=1.5, size=rel(1.5))) +
  theme(panel.background = element_rect(fill = "white", colour = "black")) +
#  theme(panel.grid.minor.y = element_line(colour = "grey50")) +
  stat_summary(fun.y=mean, geom="point", shape=18,
               size=3, color="red")

#Exportar la imagen a eps - prepare el archivo. 
setEPS()
postscript("rq1-m1-both.eps", onefile=FALSE, height=6, width=8, pointsize=10)
print(bTwoCS)
dev.off()

#Exportar la imagen a png - prepare el archivo. 
png(filename = "rq1-m1-both.png", width = 1800, height = 1200, units = "px", 
     res = 200)

#Exportar la imagen a png - genere el archivo. 
print(bTwoCS)
dev.off()

#Exportar la imagen a pdf - prepare el archivo
pdf("rq1-m1-both.pdf")
print(bTwoCS)
dev.off()

###############################################################
###   Test de Kolmogorov - Smirnov de las distribuciones    ### 
###############################################################

# MSG-B
KsMSGB<- ks.test(datosMSGB$numCA, "pnorm", mean =datosMSGB$numCA, sd= datosMSGB$numCA)

# MSG-B
KsMSGBU<- ks.test(datosMSGBU$numCA, "pnorm", mean =datosMSGBU$numCA, sd= datosMSGBU$numCA)

# MSG-B
KsMSGUC<- ks.test(datosMSGUC$numCA, "pnorm", mean =datosMSGUC$numCA, sd= datosMSGUC$numCA)

# MSG-B
KsPSB<- ks.test(datosPSB$numCA, "pnorm", mean =datosPSB$numCA, sd= datosPSB$numCA)

# MSG-B
KsPSBU<- ks.test(datosPSBU$numCA, "pnorm", mean =datosPSBU$numCA, sd= datosPSBU$numCA)

# MSG-B
KsPSUC<- ks.test(datosPSUC$numCA, "pnorm", mean =datosPSUC$numCA, sd= datosPSUC$numCA)

# MSG-B
KsBothB<- ks.test(datosBothB$numCA, "pnorm", mean =datosBothB$numCA, sd= datosBothB$numCA)

# MSG-B
KsBothBU<- ks.test(datosBothBU$numCA, "pnorm", mean =datosBothBU$numCA, sd= datosBothBU$numCA)

# MSG-B
KsBothUC<- ks.test(datosBothUC$numCA, "pnorm", mean =datosBothUC$numCA, sd= datosBothUC$numCA)

###############################################################
###   Test de Shapiro Wilk de las distribuciones            ### 
###############################################################

# MSG-B
SwMSGB<- shapiro.test(datosMSGB$numCA)

# MSG-BU
SwMSGBU<- shapiro.test(datosMSGBU$numCA)

# MSG-UC
SwMSGUC<- shapiro.test(datosMSGUC$numCA)

# PS-B
SwPSB<- shapiro.test(datosPSB$numCA)

# PS-BU
SwPSBU<- shapiro.test(datosPSBU$numCA)

# PS-UC
SwPSUC<- shapiro.test(datosPSUC$numCA)

# Both-B
SwBothB<- shapiro.test(datosBothB$numCA)

# Both-BU
SwBothBU<- shapiro.test(datosBothBU$numCA)

# Both-UC
SwBothUC<- shapiro.test(datosBothUC$numCA)

###############################################################
###   Datos de cada distribución - Summary                  ### 
###############################################################

## Summary of the data - MSG 
FileSummaryMSGB = summary(datosMSGB)
FileSummaryMSGBU = summary(datosMSGBU)
FileSummaryMSGUC = summary(datosMSGUC)

## Summary of the data - PdV
FileSummaryPdVB = summary(datosPSB)
FileSummaryPdVBU = summary(datosPSBU)
FileSummaryPdVUC= summary(datosPSUC)

# Summary of the data - BothCS 
FileSummaryBothB = summary(datosBothB)
FileSummaryBothBU = summary(datosBothBU)
FileSummaryBothUC = summary(datosBothUC)

###############################################################
###   Analisis de Varianza  -AOV                            ### 
###############################################################

# AOV de caso de estudio FMSG
FMSG.aov <- aov(numCA ~ ArtifactsSet, data = datosFMSG)

# Summary of the analysis
FileSummaryAoVFMSG = summary(FMSG.aov)

# AOV de caso de estudio PdV
PdV.aov <- aov(numCA ~ ArtifactsSet, data = datosPdV)

# Summary of the analysis
FileSummaryAoVPdV = summary(PdV.aov)

# AOV de datos ambos casos de estudio. 
TwoCS.aov <- aov(numCA ~ ArtifactsSet, data = datosTwoCS)

# Summary of the analysis
FileSummaryAovTwoCS = summary(TwoCS.aov)

###############################################################
###   File output creation                                  ### 
###############################################################

# Title
cat("Statistical data for number of correct answers", file = "numCAsc4y5-Aov-Summ.doc")
# add 2 newlines
cat("\n\n", file = "numCAsc4y5-Aov-Summ.doc", append = TRUE)

# export KS test output for MSG-B 
cat("KS test for MSG-B\n", file = "numCAsc4y5-Aov-Summ.doc", append = TRUE)
capture.output(KsMSGB , file = "numCAsc4y5-Aov-Summ.doc", append = TRUE)
# add 2 newlines
cat("\n\n", file = "numCAsc4y5-Aov-Summ.doc", append = TRUE)

# export SW test output for MSG-B 
cat("SW test for MSG-B\n", file = "numCAsc4y5-Aov-Summ.doc", append = TRUE)
capture.output(SwMSGB , file = "numCAsc4y5-Aov-Summ.doc", append = TRUE)
# add 2 newlines
cat("\n\n", file = "numCAsc4y5-Aov-Summ.doc", append = TRUE)

# export summary FMSG output
cat("Summary MSG-B\n", file = "numCAsc4y5-Aov-Summ.doc", append = TRUE)
capture.output(FileSummaryMSGB, file = "numCAsc4y5-Aov-Summ.doc", append = TRUE)
# add 2 newlines
cat("\n\n", file = "numCAsc4y5-Aov-Summ.doc", append = TRUE)

# export KS test output for MSG-BU
cat("KS test for MSG-BU\n", file = "numCAsc4y5-Aov-Summ.doc", append = TRUE)
capture.output(KsMSGBU , file = "numCAsc4y5-Aov-Summ.doc", append = TRUE)
# add 2 newlines
cat("\n\n", file = "numCAsc4y5-Aov-Summ.doc", append = TRUE)

# export SW test output for MSG-BU
cat("SW test for MSG-BU\n", file = "numCAsc4y5-Aov-Summ.doc", append = TRUE)
capture.output(SwMSGBU , file = "numCAsc4y5-Aov-Summ.doc", append = TRUE)
# add 2 newlines
cat("\n\n", file = "numCAsc4y5-Aov-Summ.doc", append = TRUE)

# export summary FMSG output
cat("Summary MSG-BU\n", file = "numCAsc4y5-Aov-Summ.doc", append = TRUE)
capture.output(FileSummaryMSGBU, file = "numCAsc4y5-Aov-Summ.doc", append = TRUE)
# add 2 newlines
cat("\n\n", file = "numCAsc4y5-Aov-Summ.doc", append = TRUE)

# export KS test output for MSG-UC
cat("KS test for MSG-UC\n", file = "numCAsc4y5-Aov-Summ.doc", append = TRUE)
capture.output(KsMSGUC, file = "numCAsc4y5-Aov-Summ.doc", append = TRUE)
# add 2 newlines
cat("\n\n", file = "numCAsc4y5-Aov-Summ.doc", append = TRUE)

# export SW test output for MSG-UC
cat("SW test for MSG-UC\n", file = "numCAsc4y5-Aov-Summ.doc", append = TRUE)
capture.output(SwMSGUC, file = "numCAsc4y5-Aov-Summ.doc", append = TRUE)
# add 2 newlines
cat("\n\n", file = "numCAsc4y5-Aov-Summ.doc", append = TRUE)

# export summary FMSG output
cat("Summary MSG-UC\n", file = "numCAsc4y5-Aov-Summ.doc", append = TRUE)
capture.output(FileSummaryMSGUC, file = "numCAsc4y5-Aov-Summ.doc", append = TRUE)
# add 2 newlines
cat("\n\n", file = "numCAsc4y5-Aov-Summ.doc", append = TRUE)

# export anova test FMSG output
cat("Anova Test FMSG\n", file = "numCAsc4y5-Aov-Summ.doc", append = TRUE)
capture.output(FileSummaryAoVFMSG, file = "numCAsc4y5-Aov-Summ.doc", append = TRUE)
# add 2 newlines
cat("\n\n", file = "numCAsc4y5-Aov-Summ.doc", append = TRUE)

# export KS test output for PdV-B
cat("KS test for PS-B\n", file = "numCAsc4y5-Aov-Summ.doc", append = TRUE)
capture.output(KsPSB, file = "numCAsc4y5-Aov-Summ.doc", append = TRUE)
# add 2 newlines
cat("\n\n", file = "numCAsc4y5-Aov-Summ.doc", append = TRUE)

# export SW test output for PdV-B
cat("SW test for PS-B\n", file = "numCAsc4y5-Aov-Summ.doc", append = TRUE)
capture.output(SwPSB, file = "numCAsc4y5-Aov-Summ.doc", append = TRUE)
# add 2 newlines
cat("\n\n", file = "numCAsc4y5-Aov-Summ.doc", append = TRUE)

# export summary PdV output
cat("Summary PS-BV\n", file = "numCAsc4y5-Aov-Summ.doc", append = TRUE)
capture.output(FileSummaryPdVB, file = "numCAsc4y5-Aov-Summ.doc", append = TRUE)
# add 2 newlines
cat("\n\n", file = "numCAsc4y5-Aov-Summ.doc", append = TRUE)

# export KS test output for PdV-BU
cat("KS test for PS-BU\n", file = "numCAsc4y5-Aov-Summ.doc", append = TRUE)
capture.output(KsPSBU , file = "numCAsc4y5-Aov-Summ.doc", append = TRUE)
# add 2 newlines
cat("\n\n", file = "numCAsc4y5-Aov-Summ.doc", append = TRUE)

# export SW test output for PdV-BU
cat("SW test for PS-BU\n", file = "numCAsc4y5-Aov-Summ.doc", append = TRUE)
capture.output(SwPSBU , file = "numCAsc4y5-Aov-Summ.doc", append = TRUE)
# add 2 newlines
cat("\n\n", file = "numCAsc4y5-Aov-Summ.doc", append = TRUE)

# export summary PdV output
cat("Summary PS-BU\n", file = "numCAsc4y5-Aov-Summ.doc", append = TRUE)
capture.output(FileSummaryPdVBU, file = "numCAsc4y5-Aov-Summ.doc", append = TRUE)
# add 2 newlines
cat("\n\n", file = "numCAsc4y5-Aov-Summ.doc", append = TRUE)

# export KS test output for PdV-UC
cat("KS test for PS-UC\n", file = "numCAsc4y5-Aov-Summ.doc", append = TRUE)
capture.output(KsPSUC, file = "numCAsc4y5-Aov-Summ.doc", append = TRUE)
# add 2 newlines
cat("\n\n", file = "numCAsc4y5-Aov-Summ.doc", append = TRUE)

# export SW test output for PdV-UC
cat("SW test for PS-UC\n", file = "numCAsc4y5-Aov-Summ.doc", append = TRUE)
capture.output(SwPSUC, file = "numCAsc4y5-Aov-Summ.doc", append = TRUE)
# add 2 newlines
cat("\n\n", file = "numCAsc4y5-Aov-Summ.doc", append = TRUE)

# export summary PdV output
cat("Summary PS-UC\n", file = "numCAsc4y5-Aov-Summ.doc", append = TRUE)
capture.output(FileSummaryPdVUC, file = "numCAsc4y5-Aov-Summ.doc", append = TRUE)
# add 2 newlines
cat("\n\n", file = "numCAsc4y5-Aov-Summ.doc", append = TRUE)

# export anova test PdV output
cat("Anova Test PdV\n", file = "numCAsc4y5-Aov-Summ.doc", append = TRUE)
capture.output(FileSummaryAoVPdV, file = "numCAsc4y5-Aov-Summ.doc", append = TRUE)
# add 2 newlines
cat("\n\n", file = "numCAsc4y5-Aov-Summ.doc", append = TRUE)

# export KS test output for Both-B
cat("KS test for Both-B\n", file = "numCAsc4y5-Aov-Summ.doc", append = TRUE)
capture.output(KsBothB , file = "numCAsc4y5-Aov-Summ.doc", append = TRUE)
# add 2 newlines
cat("\n\n", file = "numCAsc4y5-Aov-Summ.doc", append = TRUE)

# export SW test output for Both-B
cat("SW test for Both-B\n", file = "numCAsc4y5-Aov-Summ.doc", append = TRUE)
capture.output(SwBothB , file = "numCAsc4y5-Aov-Summ.doc", append = TRUE)
# add 2 newlines
cat("\n\n", file = "numCAsc4y5-Aov-Summ.doc", append = TRUE)

# export summary Both output
cat("Summary Both-B\n", file = "numCAsc4y5-Aov-Summ.doc", append = TRUE)
capture.output(FileSummaryBothB, file = "numCAsc4y5-Aov-Summ.doc", append = TRUE)
# add 2 newlines
cat("\n\n", file = "numCAsc4y5-Aov-Summ.doc", append = TRUE)

# export KS test output for Both-BU
cat("KS test for Both-BU\n", file = "numCAsc4y5-Aov-Summ.doc", append = TRUE)
capture.output(KsBothBU, file = "numCAsc4y5-Aov-Summ.doc", append = TRUE)
# add 2 newlines
cat("\n\n", file = "numCAsc4y5-Aov-Summ.doc", append = TRUE)

# export SW test output for Both-BU
cat("SW test for Both-BU\n", file = "numCAsc4y5-Aov-Summ.doc", append = TRUE)
capture.output(SwBothBU, file = "numCAsc4y5-Aov-Summ.doc", append = TRUE)
# add 2 newlines
cat("\n\n", file = "numCAsc4y5-Aov-Summ.doc", append = TRUE)

# export summary Both output
cat("Summary Both-BU\n", file = "numCAsc4y5-Aov-Summ.doc", append = TRUE)
capture.output(FileSummaryBothBU, file = "numCAsc4y5-Aov-Summ.doc", append = TRUE)
# add 2 newlines
cat("\n\n", file = "numCAsc4y5-Aov-Summ.doc", append = TRUE)

# export KS test output for Both-UC
cat("KS test for Both-UC\n", file = "numCAsc4y5-Aov-Summ.doc", append = TRUE)
capture.output(KsBothUC, file = "numCAsc4y5-Aov-Summ.doc", append = TRUE)
# add 2 newlines
cat("\n\n", file = "numCAsc4y5-Aov-Summ.doc", append = TRUE)

# export SW test output for Both-UC
cat("SW test for Both-UC\n", file = "numCAsc4y5-Aov-Summ.doc", append = TRUE)
capture.output(SwBothUC, file = "numCAsc4y5-Aov-Summ.doc", append = TRUE)
# add 2 newlines
cat("\n\n", file = "numCAsc4y5-Aov-Summ.doc", append = TRUE)

# export summary Both output
cat("Summary Both-UC\n", file = "numCAsc4y5-Aov-Summ.doc", append = TRUE)
capture.output(FileSummaryBothUC, file = "numCAsc4y5-Aov-Summ.doc", append = TRUE)
# add 2 newlines
cat("\n\n", file = "numCAsc4y5-Aov-Summ.doc", append = TRUE)

# export anova test TwoCS output
cat("Anova Test Both\n", file = "numCAsc4y5-Aov-Summ.doc", append = TRUE)
capture.output(FileSummaryAovTwoCS, file = "numCAsc4y5-Aov-Summ.doc", append = TRUE)
