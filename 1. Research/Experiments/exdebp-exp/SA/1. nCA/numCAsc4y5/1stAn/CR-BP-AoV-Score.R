# Set path to Desktop

# Para MAC
#setwd("~/proys/0-olvegamphd/1. Research/Experiments/exdebp-exp/SA/Score/Score4y5")

# Para Windows
setwd("D:/proys/0-olvegamphd/1. Research/Experiments/exdebp-exp/SA/Score/Score4y5")

# Instalacion de paquetes y librerias

#Para leer archivos de Excel
#install.packages("tidyverse")
# Leer el paquete para lectura de archivos de Excel. 
library("readxl")

#Para utilizar ggplot y graficar los boxplot con t?tulos y dem?s. 
#install.packages("ggplot2")
library(ggplot2)

##############################################################
###   Leer los datos (cada caso y los de ambos casos)      ###
##############################################################

# Lea los datos para el caso de estudio FMSG.
datosFMSG <- read_excel("Score4y5.xlsx", sheet = 2)
# Lea los datos para el caso de estudio PdV.
datosPdV <- read_excel("Score4y5.xlsx", sheet = 3)
# Lea los datos para ambos casos de estudio.
datosTwoCS <- read_excel("Score4y5.xlsx", sheet = 1)
# Lea los datos para ambos casos de estudio, distribuciones B and C.
#datosTwoCSBandC <- read_excel("CR-Subjects.xlsx", sheet = 3)


###############################################################
###   Mostrar una porcion aleatoria de cada conjunto de datos.# 
###############################################################

# Show first a random sample of each data set. 
set.seed(1234)

# Summary of the data 
dplyr::sample_n(datosFMSG, 10)
summary(datosFMSG)

# Summary of the data 
dplyr::sample_n(datosPdV, 10)
summary(datosPdV)

# Summary of the data 
dplyr::sample_n(datosTwoCS, 10)
summary(datosTwoCS)

# Summary of the data 
#dplyr::sample_n(datosTwoCS, 10)
#summary(datosTwoCSBandC)
###############################################################
###   Organice los niveles de los datos.                    ### 
###############################################################

#Asigne los niveles a la columna que los tiene (en este caso ArtifactsSet) - datos FMSG
datosFMSG$ArtifactsSet <- ordered(datosFMSG$ArtifactsSet, levels(factor(datosFMSG$ArtifactsSet)))
#show the levels
levels(datosFMSG$ArtifactsSet)

#Asigne los niveles a la columna que los tiene (en este caso ArtifactsSet) - datos PdV
datosPdV$ArtifactsSet <- ordered(datosPdV$ArtifactsSet, levels(factor(datosPdV$ArtifactsSet)))
#show the levels
levels(datosPdV$ArtifactsSet)

#Asigne los niveles a la columna que los tiene (en este caso ArtifactsSet) - datos TwoCS
datosTwoCS$ArtifactsSet <- ordered(datosTwoCS$ArtifactsSet, levels(factor(datosTwoCS$ArtifactsSet)))

#show the levels
#levels(datosTwoCS$ArtifactsSet)

#Asigne los niveles a la columna que los tiene (en este caso ArtifactsSet) - datos TwoCSBandC
#datosTwoCSBandC$ArtifactsSet <- ordered(datosTwoCSBandC$ArtifactsSet, levels(factor(datosTwoCSBandC$ArtifactsSet)))

#show the levels
#levels(datosTwoCSBandC$ArtifactsSet)

###############################################################
###   Muestre los boxplot de cada conjunto de datos.        ### 
###############################################################

#Dibuje el boxplot de FMSG (Diagrama de cajas)
bMSG = ggplot(datosFMSG, aes(ArtifactsSet,Score)) + geom_boxplot(outlier.shape = 19) +
  ggtitle ("Case study credit application on FMSG") +
  labs(x = "Artifacts set", y= "Score") + 
  theme (plot.title = element_text(vjust=2, hjust=0.5, lineheight=1.5, size = 20))  +
  theme(axis.title.x = element_text(face="bold", vjust=-0.5, size=rel(1.5))) +
  theme(axis.title.y = element_text(face="bold", vjust=1.5, size=rel(1.5))) +
  theme(axis.text.x = element_text(face="bold", vjust=1.5, size=rel(1.5))) +
  theme(axis.text.y = element_text(face="bold", vjust=1.5, size=rel(1.5))) +
  theme(panel.background = element_rect(fill = "white", colour = "black")) +
  theme(panel.grid.minor.y = element_line(colour = "grey50")) +
  stat_summary(fun.y=mean, geom="point", shape=18,
               size=3, color="red")

#Exportar la imagen a eps - prepare el archivo. 
setEPS()
postscript("BoxPlot-MSG-Score4y5.eps", onefile=FALSE, height=6, width=8, pointsize=10)
print(bMSG)
dev.off()

#Exportar la imagen a png - prepare el archivo. 
png(filename = "BoxPlot-MSG-Score4y5.png", width = 1800, height = 1200, units = "px", 
    res = 200)

#Exp?rtelo al tipo de archivo preparado
print(bMSG)
dev.off()

#Dibuje el boxplot de PdV. (Diagrama de cajas)
bPdV=ggplot(datosPdV, aes(ArtifactsSet,Score)) + geom_boxplot() +
  ggtitle ("Case study point of sale") + 
  labs(x = "Artifacts set", y= "Score") + 
  theme (plot.title = element_text(vjust=2, hjust=0.5, lineheight=1.5, size = 20))  +
  theme(axis.title.x = element_text(face="bold", vjust=-0.5, size=rel(1.5))) +
  theme(axis.title.y = element_text(face="bold", vjust=1.5, size=rel(1.5))) +
  theme(axis.text.x = element_text(face="bold", vjust=1.5, size=rel(1.5))) +
  theme(axis.text.y = element_text(face="bold", vjust=1.5, size=rel(1.5))) +
  theme(panel.background = element_rect(fill = "white", colour = "black")) +
  theme(panel.grid.minor.y = element_line(colour = "grey50")) +
  stat_summary(fun.y=mean, geom="point", shape=18,
               size=3, color="red")

#Exportar la imagen a eps - prepare el archivo. 
setEPS()
postscript("BoxPlot-PdV-Score4y5.eps", onefile=FALSE, height=8, width=10, pointsize=10)
print(bPdV)
dev.off()

#Exportar la imagen a png - prepare el archivo. 
png(filename = "BoxPlot-PdV-Score4y5.png", width = 1800, height = 1200, units = "px", 
    res = 200)
print(bPdV)
dev.off()

#Dibuje el boxplot de ambos casos de estudio. (Diagrama de cajas)
bTwoCS = ggplot(datosTwoCS, aes(ArtifactsSet,Score)) + geom_boxplot()+
 ggtitle ("Two case studies data") +
  labs(x = "Artifacts set", y= "Score") + 
  theme (plot.title = element_text(vjust=2, hjust=0.5, lineheight=1.5, size = 20))  +
  theme(axis.title.x = element_text(face="bold", vjust=-0.5, size=rel(1.5))) +
  theme(axis.title.y = element_text(face="bold", vjust=1.5, size=rel(1.5))) +
  theme(axis.text.x = element_text(face="bold", vjust=1.5, size=rel(1.5))) +
  theme(axis.text.y = element_text(face="bold", vjust=1.5, size=rel(1.5))) +
  theme(panel.background = element_rect(fill = "white", colour = "black")) +
  theme(panel.grid.minor.y = element_line(colour = "grey50")) +
  stat_summary(fun.y=mean, geom="point", shape=18,
               size=3, color="red")

#Exportar la imagen a eps - prepare el archivo. 
setEPS()
postscript("BoxPlot-TwoCS-Score4y5.eps", onefile=FALSE, height=6, width=8, pointsize=10)
print(bTwoCS)
dev.off()

#Exportar la imagen a png - prepare el archivo. 
png(filename = "BoxPlot-TwoCS-Score4y5.png", width = 1800, height = 1200, units = "px", 
     res = 200)

#Exportar la imagen a png - genere el archivo. 
print(bTwoCS)
dev.off()

###############################################################
###   Analisis de Varianza  -AOV                            ### 
###############################################################

# AOV de caso de estudio FMSG
FMSG.aov <- aov(Score ~ ArtifactsSet, data = datosFMSG)

# Summary of the analysis
FileSummaryFMSG = summary(FMSG.aov)

# AOV de caso de estudio PdV
PdV.aov <- aov(Score ~ ArtifactsSet, data = datosPdV)

# Summary of the analysis
FileSummaryPdV = summary(PdV.aov)

# AOV de datos ambos casos de estudio. 
TwoCS.aov <- aov(Score ~ ArtifactsSet, data = datosTwoCS)

# Summary of the analysis
FileSummaryTwoCS = summary(TwoCS.aov)

# AOV de distribucion ArtifactsSet B and C. 
#TwoCSBandC.aov <- aov(Score ~ ArtifactsSet, data = datosTwoCSBandC)

# Summary of the analysis
#FileSummaryTwoCSBandC = summary(TwoCSBandC.aov)
###############################################################
###   File output creation                                  ### 
###############################################################

# Title
cat("Anova summaries procedure questions output", file = "Score4y5-Aov-Summ.doc")
# add 2 newlines
cat("\n\n", file = "Score4y5-Aov-Summ.doc", append = TRUE)
# export anova test FMSG output
cat("Anova Test FMSG\n", file = "Score4y5-Aov-Summ.doc", append = TRUE)
capture.output(FileSummaryFMSG, file = "Score4y5-Aov-Summ.doc", append = TRUE)
# add 2 newlines
cat("\n\n", file = "Score4y5-Aov-Summ.doc", append = TRUE)
# export anova test PdV output
cat("Anova Test PdV\n", file = "Score4y5-Aov-Summ.doc", append = TRUE)
capture.output(FileSummaryPdV, file = "Score4y5-Aov-Summ.doc", append = TRUE)
# add 2 newlines
cat("\n\n", file = "Score4y5-Aov-Summ.doc", append = TRUE)
# export anova test TwoCS output
cat("Anova Test TwoCS\n", file = "Score4y5-Aov-Summ.doc", append = TRUE)
capture.output(FileSummaryTwoCS, file = "Score4y5-Aov-Summ.doc", append = TRUE)
# add 2 newlines
#cat("\n\n", file = "Score4y5-Aov-Summ.doc", append = TRUE)
# export anova test TwoCS output
#cat("Anova Test TwoCS for B and C only \n", file = "Score4y5-Aov-Summ.doc", append = TRUE)
#capture.output(FileSummaryTwoCSBandC, file = "Score4y5-Aov-Summ.doc", append = TRUE)
