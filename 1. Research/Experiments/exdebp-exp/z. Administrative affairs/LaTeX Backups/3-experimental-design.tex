%% Experimental Design
%% ===================

\section{Experimental Design}
\label{sec:design}

We are interested on studying the benefits of using process models by software developers for eliciting and comprehending software requirements. To achieve this goal, we designed an experiment that measures whether the usage of BPMN  models improve requirements comprehension when compared to traditional artifacts such as use cases specification.  
%% Motivation

We expected the experiment (i) to expand the research about the contribution of
BPMN in comprehension of business domain issues~\cite{Ottensooser:2012} and business processes~\cite{Rodrigues:2015},
to cover the software requirements comprehension such as it has been suggested by
some experiences in software development~\cite{bmsd17}. Similarly, we expected (ii) to verify
in terms of time, that users locate a requirement more easily in BPM models than in
use cases~\cite{Haisjackl:2014}.

% We expected the experiment to confirm the following traditional insights and beliefs regarding the usage of BPMN and UML models: (i) BPMN helps developers to understand the process and general requirements for a software system, and use cases help to understand specific requirements~\cite{bmsd17}; (ii) users prefer the graphical notation of BPMN against the textual descriptions in the use cases~\cite{Ottensooser:2012}\cite{Rodrigues:2015};  (iii) users locate a requirement more easily in BPM models than in use cases~\cite{Haisjackl:2014}, and (iv) a combination of both instruments could improve the understanding of requirements for software supporting processes~\cite{bmsd17}. 

%% Foundations

The experiment was designed by following the Goal/Question/Metric paradigm (GQM)~\cite{basili:1994} and the learned lessons presented by Gross et al.~\cite{Gross:2012}, where:
(i) the research questions are stated as research goals (G),
(ii) questions (Q) are defined to characterize the assessment/achievement of each goal, and (iii) data metrics (M) are associated to each question to answer it in a quantitative way.  

Table \ref{tab:gqm-goal} shows the overall GQM design for our experiment, based on the template proposed by Basili~\cite{basili:1994}.
We defined, as the overall goal (G), \textit{Analyze and Compare the software requirements comprehension achieved when using BPMN models and use case specifications}. We subcategorized this general goal in two specific goals (see Table \ref{tab:gqm-goal}). These goals and the corresponding metrics were used to design the experiment and analyze the results.

\input{content/3-x-table-GQM-design.tex}


%% Design
%% ======

%\subsection{Design of the Experiment}

 
%Afterwards, we describe what case studies we used, how the samples were taken and how the data analysis was carried out. 


\subsection{Case studies}

We provided participants with a case study description and different modeling artifacts. The participants were expected to perform a comprehension task, \ie read a case study and artifacts, and then answering a questionnaire with control-type questions to identify whether the participants are able to comprehend the software requirements. In addition, for each question we recorded the time spent by the participant to answer each question (this feature is provided by the online tool we used to create and publish the questionaire). 

We prepared two case studies describing two different software systems that are not implemented yet. For each one, we defined (i) a case study description, (ii) a BPMN model representing the process depicted in the case study, and (iii) a set of use cases specification describing the sotfware requirements.
Each description was designed to be printed in less than a page and have an average reading time of less than 10 minutes, to reduce early drop outs of the participants.
The BPMN models were created  using the BPMN 2.0  notation, \ie using elements and attributes supporting advanced events and errors. 
The use cases were specified using the Kettenis's template~\cite{Kettenis:2007}, that resulted as the most comprehensive use case format in a recent survey~\cite{Tiwari:2017}.

During the design of the experiments, we prepared six case studies, and after several discussions (among the authors), we selected the two most appropriated for the experiment. The two case studies were selected considering the number of elements in the model (\ie selecting the cases with the same number of lanes and participants), the expected time to review all the artifacts and the ability to answer the same questions from both the BPMN and the use cases.

The two selected case studies were  a credit application system in the MSG Foundation  ({\tt MSG}), and  a Point-of-Sale system in a supermarket  ({\tt PS}). The former is an academic case study used to teach software engineering by Schach~\cite{Schach:2010}, and the latter is one of the case studies used to compare use case formats in a recent survey by Tiwari and Gupta~\cite{Tiwari:2017}. 
We modified the original case studies to fit the time conditions of the experiment and traslated the text to Spanish. Note that the experiments were expected to be finished by the participants in about 40 minutes. Therefore, we tested and modified the case studies descriptions to be read by the participants in a maximum time of 10 minutes. Tables~\ref{tab:BPMN-Char} and~\ref{tab:UC-Char} summarize the number of notational elements included in the corresponding BPMN models and use cases. It is worth noting that we created the BPMN models from scratch (for both case studies), and the use cases specification were adjusted by us to the experiment needs and translated to Spanish\footnote{The original use case specifications were written in English by Schach~\cite{Schach:2010} and Tiwari and Gupta~\cite{Tiwari:2017}.}. 

\input{content/3-x-table-BPMN-characteristics.tex}

\input{content/3-x-table-UC-characteristics.tex}

\textit{Questionnaire}. 
We built a questionnaire for each case study considering the objectives defined in the GQM design questions (\S Table \ref{tab:gqm-goal}) and the above mentioned case studies and artifacts (\ie BPMN models and use cases specification).
For evaluating the comprehension level (Q1) , we designed 11 questions, combining free text, multiple selection and true/false questions. 
Regarding the comprehension time (Q2), we measured the time each participant spent in answering each comprehension question.

For simplicity, hereinafter we will refer each sample by using labels that describe both the case study and the artifacts. 
We use \texttt{MSG} for the MSG foundation case study and \texttt{PS} for the Point of Sale case study. 
In addition, for the artifacts: \texttt{B} for the BPMN model, \texttt{UC} for the use case specification and \texttt{BU} for the combination. 
For example,  the \texttt{MSG-BU} label represents the sample from the participants group that received the MSG Foundation case study, a BPMN model and a use case specification. In the same way, \texttt{PS-B} represents the group that received the Point of Sale case study and a BPMN model.

\textit{Pilot test.} 
Before running the experiment with participants, we applied seven pilot tests with volunteers that have experience in both BPMN and use case specifications. The volunteers group was conformed by Systems and Computing Engineers with at least two years of experience in software development, and faculty members in the Universities where the experiment was applied. The volunteers reviewed the artifacts and provided us with comments for improving the case studies description, the artifacts, and the questionnaire. With the provided feedback we improved (i) the correctness of the artifacts, (ii) the coherence between the case study descriptions, (iii) the BPMN models and the use case specifications, and (iv) the quality of the questions (\eg we reduced terms ambiguity).



%% Samples
%% =======

\subsection{Participants distribution}

The experiment was applied on courses belonging to different universities in Colombia; hereinafter we will reffer to the participants group from each course as a sample. In particular, students from three universities participated in the experiment:  Universidad Nacional de Colombia,  Universidad de los Llanos,  and  Universidad de los Andes.  We applied the experiment in 10 samples, accounting for a total of 137 participants. The answers from 12 participants were discarded because they did not finish the questionnaire or had invalid responses.  Finally, to balance the participants across the possible combinations of case study and artifact (\eg, {\tt PS}+ BPMN model), we randomly selected 20 participants for each combination, among the remaining 125 participants. Thus, at the end we analyzed the responses from 120 participants.

The participants were distributed in three groups, each one receiving the same case study (just one of the two constructed) and questions, but receiving a different set of artifacts: (i) a control group provided with the BPMN model, (ii) a treatment group provided with the use case specification,  and (iii) a second treatment group provided with both BPMN model and the use case specification. 

The samples had undergraduate and graduate students from Engineering programs. While the students from Systems and Computing Engineering (SCE) are knowledgeable of both BPMN and  use cases specification, the non-SCE  students only had knowledge of BPMN. Note that in the non-SCE sample we applied the experiment only with BPMN artifacts.



Finally, the two case studies and the groups were balanced in the way the \tabref{tab:SampleDescription} shows. 
We considered 60 responses for each case study, \ie 20 response for each set of artifacts combination. 

\begin{table}[htbp]
	\centering
	\caption{Balanced sample characteristics}
	\resizebox{11.5cm}{!}{
	\begin{tabular}{cccccccrcc}
		\toprule
		
		\multirow{3}[6]{*}{\textbf{Poll \#}} & \multicolumn{2}{c}{\textbf{Origin}} & \multicolumn{6}{c}{\textbf{Case Study - Artifacts Set}} & \multirow{3}[6]{*}{\textbf{\enskip TOTAL\enskip}} \\
		
		\cmidrule{2-9}          & \multirow{2}[4]{*}{\textbf{Institution}} & \multirow{2}[4]{*}{\textbf{Program}} & \multicolumn{3}{c}{\textbf{MSG}} & \multicolumn{3}{c}{\textbf{PS}} &  \\
		
		\cmidrule{4-9}          &       &       & \textbf{B} & \textbf{BU} & \textbf{UC} & \textbf{B} & \multicolumn{1}{r}{\textbf{BU}} & \textbf{UC} &  \\
		
		%\cmidrule{1-9}
		\midrule
		
		1     & \multicolumn{1}{l}{Unal} & \multicolumn{1}{l}{Undergrad - CS and Industrial Eng.} & 3     & 3     & 2     & 3     & 2     & \multicolumn{1}{c}{3}    & 16 \\
		2     & \multicolumn{1}{l}{Uniandes} & \multicolumn{1}{l}{Graduate - Spec. Softw. Construction } & 3     & 3     & 3     & 3     & 3     & 2     & 17 \\
		3     & \multicolumn{1}{l}{Uniandes} & \multicolumn{1}{l}{Graduate - M.Sc. in Computing. } & 2     & 4     & 3     & 3     & 2     & 1     & 15 \\
		4     & \multicolumn{1}{l}{Uniandes} & \multicolumn{1}{l}{Graduate - M.Sc. in Computing. } & 3     & 3     & 3     & 3     & 3     & 3     & 18 \\
		5     & \multicolumn{1}{l}{Uniandes} & \multicolumn{1}{l}{Undergrad - CS, Indust Eng and others. } & 7     & 0     & 0     & 7     & 0     & 0     & 14 \\
		6     & \multicolumn{1}{l}{Unillanos} & \multicolumn{1}{l}{Graduate - Spec. Softw. Eng.} & 0     & 1     & 4     & 0     & 2     & 4     & 11 \\
		7     & \multicolumn{1}{l}{Unillanos} & \multicolumn{1}{l}{Undergrad - CS} & 0     & 1     & 1     & 0     & 1     & 4     & 7 \\
		8     & \multicolumn{1}{l}{Unillanos} & \multicolumn{1}{l}{Undergrad - CS} & 2     & 2     & 1     & 1     & 2     & 1     & 9 \\
		9     & \multicolumn{1}{l}{Unillanos} & \multicolumn{1}{l}{Undergrad - CS} & 0     & 3     & 3     & 0     & 5     & 2     & 13 \\
		\midrule
		\multicolumn{3}{c}{\textbf{TOTALS}} & 20    & 20    & 20    & 20    & 20    & 20    & 120 \\
		\midrule
		\multicolumn{3}{c}{\textbf{TOTALS PER CASE STUDY}} & \multicolumn{3}{c}{60} & \multicolumn{3}{c}{60} & 120 \\
		\bottomrule
	\end{tabular}%
	}
	\label{tab:SampleDescription}%
\end{table}%

%For simplicity, hereinafter we will refer each sample by using labels that describe both the case study and the artifacts. 
%We used \texttt{MSG} for the MSG foundation case study and \texttt{PS} for the Point of Sale case study. 
%In addition, for the artifacts: \texttt{B} for the BPMN model, \texttt{UC} for the use case specification and \texttt{BU} for the combination. 
%Thus the \texttt{MSG-BU} label represents the sample from the participants group that received the MSG Foundation case study, a BPMN model and a use case specification. In the same way, \texttt{PS-B} represents the sample data for the group that received the Point of Sale case study and a BPMN model.



\subsection{Analysis method}

We analyzed the data in three phases: first, we conducted an \textit{exploratory data analysis} to get an overview of the results; then, we performed \textit{hypothesis significance testing} to determine whether there is a significant difference in the samples corresponding to each artifact and combination.
As part of our hypothesis significance testing, we applied first a Shapiro Wilk test \cite{ShapiroWilks:1965} or a Kolmogorov-Smirnov test \cite{CorderAndForeman:2009} to determine the data homogeneity (normality tests) of the samples.
The former is used when the sample has a size of less than 50, while the latter is used otherwise.
Then, we selected the most appropriated test for each sample, according to the parametric or non-parametric nature of the dad.
We used ANOVA test\cite{goos:2016} for parametric samples and Mann-Whitney\cite{CorderAndForeman:2009} for non-parametric ones.
Finally, when significant differences were found, we applied the Bonferroni correction procedure  \cite{CorderAndForeman:2009} and, when the significant differences were maintained, we calculated the effect size with the Cliff's d coefficient\cite{grissom:2005}. We augmented the statistical analysis with \textit{qualitative analysis} to identify differences at the question level. 

\endinput
