# Use Case Specification templates. 

##  IBM Knowledge Center
###  [Rational Requirements Composer 2.0.0] 

![IBM][UCS IBM]


##  Scott Ambler Templates
###  Simple

![ScottAmbler Simple][UCS SA-Simple]


###  Formal

![ScottAmbler Formal][UCS SA-Formal]




[Rational Requirements Composer 2.0.0]: https://www.ibm.com/support/knowledgecenter/en/SSWMEQ_2.0.0/com.ibm.rational.rrc.help.doc/topics/r_uc_spec_outline.html

[UCS IBM]: ./ESDEBP-UCS-IBM.png
[UCS SA-Simple]: ./ESDEBP-UCS-ScottAmbler-Simple.png
[UCS SA-Formal]: ./ESDEBP-UCS-ScottAmbler-Formal.png