# Fundación MSG

La Fundación MSG, creada con recursos por $2.3 billones de dólares donados por la señora Martha Stockton Greengage, apoya a matrimonios jóvenes para que adquieran vivienda propia, a través de créditos hipotecarios por el 100% del valor de la casa, a bajo costo, sin cuota inicial, con cuotas fijas y con subsidio para las cuotas que la pareja no alcance a cubrir. La Fundación otorga este crédito si se cumplen las siguientes condiciones:  

* Es un matriomonio de entre 1 y 10 años. 
* La pareja demuestra que ambos están empleados de tiempo completo desde hace por lo menos un año. 
* El precio de la vivienda es inferior al precio promedio de los últimos 12 meses de las casas en el área.  
* El valor de la cuota excede el 28% de los ingresos de la pareja (La cuota se calcula con capital de 90% del valor de la casa, tasa fija y plazo de 30 años).
* Los ahorros de la pareja son inferiores al 10% del valor de la casa. 
* La Fundación tiene fondos para comprar la casa.  

Una solicitud de crédito debe estar acompañada de los soportes respectivos. Si hay inconsistencias o errores, se informa a la pareja para que subsane la situación, en un plazo no mayor a 15 días calendario. Cumplido este plazo sin recibir los ajustes, la solicitud es eliminada. Si los soportes son válidos, el analista de crédito envía al comité de crédito y posteriormente registra el resultado de la evaluación, el cual es informado a la pareja. Si el crédito es aprobado, también se informan las instrucciones de legalización y posterior desembolso al vendedor del inmueble. El valor aprobado se descuenta del valor disponible en la Fundación para compra de vivienda. 

La Fundación determina si tiene fondos para aprobar un crédito de la siguiente forma: Divide en 12 los ingresos estimados de sus inversiones (los recursos no utilizados para financiar vivienda se invierten), le resta el estimado anual de gastos de sostenimiento dividido en 12, le suma el estimado de pagos por cuotas que recibirá este mes, y le resta el estimado de los subsidios que otorgará este mes. El saldo es distribuido entre las solicitudes de crédito que cumplen los demás requisitos. Con el dinero no utilizado en compra de viviendas se hacen inversiones redimibles en un mes.  

Si una solicitud es aprobada, se establece la cuota fija compuesta de abono a capital y a intereses. Mensualmente, al momento del pago, la pareja presenta sus soportes de ingresos del mes anterior y el máximo pago que realiza es del 28% de éstos. Si la cuota es mayor, la Fundación asume el resto como subsidio. De esta manera, el valor que paga la pareja mensualmente puede variar, dependiendo de sus ingresos. Si la pareja paga menos de lo informado, el faltante se constituye en mora y un área especializada hace el cobro. Este valor también se de los ingresos a la Fundación por concepto de pago de cuotas.  

La junta directiva de la Fundación ha contratado una firma para el desarrollo del software que soporte los procesos descritos. Una de las principales necesidades de la fundación es contar con un cálculo automático de cuántos fondos hay disponibles cada mes para la financiación de viviendas. 

NOTA: Para la realización del experimento en ESDEBP sugiero quitar la porción del pago de la cuota. 

