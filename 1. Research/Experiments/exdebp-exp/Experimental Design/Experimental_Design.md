#  [ESDEBP] Experimental design

Based on lesson learned from: http://ieeexplore.ieee.org.ezproxy.uniandes.edu.co:8080/document/6347684 

Next are the elements that compose the experiment, applying Goal/Question/Metrics paradigm

1.  Overall research goal 

_Analyze BPMN and UC for the purpose of their comparison with respect to their usefulness for requirements comprehension from the viewpoint of developers._  


2.  Sub-goals (Goal/Question/Metrics)

* **G1.** Analyze the BPMN and the UC with respect to their contribution to the requirements comprehension, which is reflected in the understability of the requirements. 
* **Q1.** Does BPMN increase correctness of requirements comprehension?  
* **M1.** Number of incorrect answers given by the participants to content-related questions referring to a given requirement.   

#####

* **G2.** Analyze the BPMN and the UC with respect to their contribution to the requirements comprehension, which is reflected in the time required to answer requirements comprehension questions. 
* **Q2.** Is the comprehension of requirements more or less time-consuming using BPMN?
* **M2.** Time required to answer the requirements comprehension questions  

#####

* **G3.** 
* **Q3.** What are the difficulties or facilities BPMN introduces to the requirements comprehension?
* **M3.**   

#####

* **G4.** 
* **Q4.** What is the level of well perception of BPMN ?
* **M4.**  

#####

* **G5.** 
* **Q5.** Are there relations among the knowledge background or experience and the utility of BPMN models for requirements comprehension?
* **M5.**  




RQ1: 
2) Time to respond.: Questions are presented one at a time.  Once a question is answered, the following will appear. In this way the time taken to answer each question is recorded and we can answer questions as:
RQ2: is the comprehension of requirements more or less time-consuming using BPMN?
3) Process carried out to respond.: Contrasting the findings in the previous questions and the data collected using the "think aloud" strategy, whe hope answer: 
RQ3: What are the difficulties or facilities BPMN introduces to the requirements comprehension?
4) Perception: With the perception questions we can discover 
RQ4: what is the level of well perception of BPMN ?
5) Bakground knowledge: Finally, background knowledge must be analized to find if 
RQ5: Are there relations among the knowledge background or experience and the utility of BPMN models for requirements comprehension?