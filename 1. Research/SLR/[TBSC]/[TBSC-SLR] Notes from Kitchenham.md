# Feature of Systematic Reviews
Some of the features that differentiate a systematic review from a conventional literature review are:
• Systematic reviews start by defining a review protocol that specifies the research
question being addressed and the methods that will be used to perform the review.
• Systematic reviews are based on a defined search strategy that aims to detect as
much of the relevant literature as possible.
• Systematic reviews document their search strategy so that readers can access its
rigour and completeness.
• Systematic reviews require explicit inclusion and exclusion criteria to assess each
potential primary study.
• Systematic reviews specify the information to be obtained from each primary
study including quality criteria by which to evaluate each primary study.
• A systematic review is a prerequisite for quantitative meta-analysis