# Traceability from BPMN to source code. 

# Research Questions

**RQ1.** What are the existing approaches to manage traceability from PBMN models to source code?

**RQ2.** What are the artifacts that can be traced in these approaches? 

**RQ3.** What are the (manual or automatic) techniques and tools proposed to manage the traces in the identified approaches?

**RQ4.** How have the approaches identified in RQ1 been evaluated, and what was the outcome?  
   
-------

