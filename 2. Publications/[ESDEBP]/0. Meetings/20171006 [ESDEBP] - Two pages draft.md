# Purpose of the meeting.

Complete the two pages summary proposal of the chapter for [ESDEBP].

# Agenda

1.  Context - ESDEPB.
2.  Review summary
3.  Select the case study for the Experiment
4.  Next Step (Strategy).

# Development

##  O.  Context - ESDEPB.

[ESDEBP]: Experimental Studies on Development of Executable Business Processes

*Book Publication* Springer.
*Topics of Interest*
* Combination of Process Elicitation and Requirements Engineering approaches
* Understandability of Executable Business Process Models by different stakeholders
*First date* 2017-10-15	Submission of a two-page summary

## 2. Review summary

The two pages proposal can be read at [ShareLateX].

###  Summary of agreemnts:
An experiment to evaluate the level of comprehension of the requirements, from a case study with a BPMN model, or with an Use Case Specification or with both.

#### Type of experiment.
Comparison of levels of comprehension of requirements, depending of the source artifacts used to describe them. The questions will be oriented to completeness and correctness.

#### Data.
Additionally we will collect data to study the reasons for the results found, by means of:
* _The "think aloud" strategy_  to study how people understand and relate the artifacts, what is the flow of analysis people make when reading the artifacts and so on.
* _Applying open questions_ about perception, facilities or difficulties found in the artifacts, to know how to improve the process for understanding requirements.

#### CaseStudy
In the Stephen Schach' book, "Classical and Object-Oriented Software Engineering", there are two case studies that can be used to the experiment:
* [MSG Foundation]: A foundation that offers financing and grants for buying a house, to newly married couples with scarce resources.
* [Chocoholics Anonymous]: An organization that offers, by an affiliation, healthcare professionals access for controlling the chocolate addiction.

#### Artifacts:
*  Text with the Case Study.
*  Use Case Specification (diagram + RUP format - from Scott Ambler:Effective Use Cases).
*  BPMN TO-BE.

#### Pilot test.
Applied to the professors
*  Andrea Herrera
*  Oscar Gonzalez

#### Population (Sample).
Master students of the professor Mario Sánchez' course.

#### Groups
1. Group 1
    *  A case study in natural language
    *  The Use Case specification

2. Group 2.
    *  A case study in natural language
    *  A BPMN model (TO-BE)

3. Group 3.
    *  A case study in natural language
    *  The Use Case specification
    *  A BPMN model (TO-BE)

#### Questionnaire
The three groups will be given the same set of questions. Some questions will contain use case **diagrams** that the participants will have to evaluate in order to decide whether or not they result from the requirements expressed in each of the delivered models (natural language form and BPMN diagrams).

The questionnaire must have demographic background.

## 2.  Select the case study for the Experiment

###  MSG Foundation
  Original:  [MSG Foundation Original]
  Summary: [MSGF-CaseStudy.md]

###  Chocoholics Anonymous

  Original: [Chocoholics Anonymous Original]
  Summary: [ChocAn-CaseStudy.md]

##  4.  Next Step (Strategy).

###  Co-authors' tasks.

| Coauthor | Task |
| --- | --- |
| Olga Lucero | <ul><li>Chapter proposal summary,</li><li>Review of editors papers.</li><li>Experiment artifacts (Study Case, Use Case Specification, BPMN TO-BE model, and Questionnaire) </li><li>Review process aware information systems,</li></ul>|
| Jaime Chavarriaga | Related work |
| Mario Sánchez | Artifacts review |
| Mario Linares Vásquez | <ul><li> Experimental Design</li><li>Format for the Use Case Specification</li> </ul>|

###  Next meeting

Somday Xxxxx # October, #:00pm - ML-### (@olvegam  reserve)

---------

#  Resources.
[ESDEBP]
[ShareLateX]
[MSG Foundation Original]
[Chocoholics Anonymous Original]

--------

[ESDEBP]: http://design.inf.usi.ch/books/ESDEBP
[ShareLateX]: https://www.sharelatex.com/project/59bc5af4e2d00d56afae1ca7
[MSG Foundation Original]: https://gitlab.com/olvegam/0-olvegamphd/blob/master/1.%20Research/Case%20Studies/MSG%20Foundation/MSG%20Foundation%20Description.pdf
[Chocoholics Anonymous Original]: https://gitlab.com/olvegam/0-olvegamphd/blob/master/1.%20Research/Case%20Studies/Chocoholics%20Anonymous/Chocoholics%20Anonymous%201.pdf
[MSGF-CaseStudy.md]: https://gitlab.com/olvegam/0-olvegamphd/blob/master/1.%20Research/Case%20Studies/MSG%20Foundation/MSGF-CaseStudy.md
[ChocAn-CaseStudy.md]: https://gitlab.com/olvegam/0-olvegamphd/blob/master/1.%20Research/Case%20Studies/Chocoholics%20Anonymous/ChocAn-CaseStudy.md
