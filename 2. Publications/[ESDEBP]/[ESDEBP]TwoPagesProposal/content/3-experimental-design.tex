%% Experimental Design
%% ===================

\section{Experimental Design}
\label{sec:design}

We are interested on studying the benefits of using process models by software developers for eliciting and comprehending software requirements. To achieve this goal, we have designed an experiment that compares the usefulness of BPMN process models and of more traditional artifacts such as requirement descriptions and Use Cases.

% For replication purposes, the experimental package (in English) and the raw data are made available on the Web~\url{}

\begin{comment}

    \subsection{Motivation} 
    
    In software development, how to achieve good communication among domain experts and software engineers remains as a challenge.  
    This is true also for developing software supporting business processes.
    Although some proposals such as BPMN aims to improve the collaboration among the people analyzing and improving the processes with the people implementing and monitoring them, these proposals usually do not consider people defining requirements for developing the software that supports that processes.
    For instance, there are few techniques to elicit the requirements from BPMN models and specify them using use cases or UML diagrams.
    We are interested on exploring if the BPMN models are a sufficient input to developers engineering requirements, if they offer a better input than other artifacts or if there is a combination of artifacts that the developers may use to comprehend the requirements and develop the corresponding software.

\end{comment}

\subsection{Motivation}
We expect the experiment to confirm the following traditional insights and beliefs regarding the usage of BPMN and UML models: (i) BPMN helps developers to understand the process and general requirements for a software system, and use cases help to understand specific requirements~\cite{bmsd17}; (ii) users prefer the graphical notation of the BPMN against the textual descriptions in the Use Cases~\cite{Ottensooser:2012}\cite{Rodrigues:2015};  (iii) users remember more easily where they observe a requirement in the BPM model than in the Use Cases~\cite{Haisjackl:2014}, and (iv) a combination of both instruments can improve the understanding of requirements for software supporting processes~\cite{bmsd17}. 


\subsection{Foundations} 

Our experiment was designed by following the Goal/Question/Metric paradigm (GQM)~\cite{basili:1994} and the learned lessons presented by Gross et al.~\cite{Gross:2012}, where:
(i) the research questions are stated as research goals (G),
(ii) questions (Q) are defined to characterize the assessment/achievement of each goal, and (iii) data metrics (M) are associated to each question to answer it in a quantitative way.  

Table \ref{tab:gqm-goal} shows the overall GQM design for our experiment, based on the template proposed by Basili~\cite{basili:1994}.
We defined, as the overall goal (G), \textit{Analyze and Compare the software requirements comprehension achieved when using BPMN models and Use Case specifications%for developing the software supporting business processes
}.
We defined four additional goals with their corresponding questions and metrics (see Table \ref{tab:gqm-goal}).

\begin{table}[t]
    \centering
    \caption{GQM Design Template for our experiment}
    \label{tab:gqm-goal}
    \vspace{-0.2cm}
\begin{tabular}{ p{0.05\textwidth} l p{0.29\textwidth} }
    
    \hline
    
    \textbf{Overall}
    & Purpose
    & Analyze and Compare
    \\
    
    \textbf{Goal}
    & Issue
    & Software Requirements Comprehension
    \\
    
    & Object
    & BPMN and Use Cases
    \\
    
    & Viewpoint
    & Developers
    \\


    \hline
    
    Goal
    & G1
    & Compare correctness during the comprehension task
    \\

    Question
    & Q1
    & Is the comprehension improved?
    \\
    
    Metrics
    & M1
    & Number of requirements incorrectly identified
    \\
    
    & M2
    & Number of requirements correctly identified
    \\
    
    \hline
    
    Goal
    & G2
    & Compare the developers effort (i.e., time) during the comprehension task
    \\

    Question
    & Q2
    & Is the performance (in terms of time) improved?
    \\
    
    Metrics
    & M3
    & Time required to answer questions related to the software requirements of the system under analysis
    \\
    
    \hline

    Goal
    & G3
    & Compare difficulties introduced by each artifact during the comprehension task
    \\

    Question
    & Q3
    & Do the elements in the artifacts improve the requirements comprehension?
    \\
    
    Metrics
    & M4 
    & Developer issues when answering questions related to the software requirements of the system under analysis.   
    \\
 
    
    \hline

    Goal
    & G4
    & Measure the impact of previous experience of the developers.
    \\

    Question
    & Q4 
    & Is there any relationship among background knowledge and correctness of the answers? 
    \\
    
    Metrics
    & M5 
    & Correlation between demographic background variables and M1, M2, and M3
    \\
    
    \hline
    
    
\end{tabular}
\vspace{-0.5cm}
\end{table}


% In the following, we present how we will run the experiment, our study material and the participants selection and organization. 

\subsection{Design of the Experiment}
%\subsection{Experiment Running}

Our experiment consists of providing participants with a case study and different modeling artifacts. The participants should perform a comprehension task, i.e., read the case study and artifacts, and then answering a questionnaire with (i) demographic background questions, (ii) control-type questions to identify whether the participants are able to comprehend the software requirements, and (iii) open questions aimed at gathering their perception about the usefulness of the artifacts. The participants are instructed to follow a "thinking aloud" protocol during the experiment time; the collected speech "notes" are manually analyzed by following an "open coding" approach~\cite{OpenCoding:2007} to analyze the participants rationale and comprehension process.


As for the case study, we use a business process description for a software system that must be implemented.
It was designed to be only one page and have a reading time of less than 10 minutes (to avoid early drop out rate).
Two additional artifacts are included: a BPMN model and an Use Case specification. The former depicts the process to be implemented, and the latter describes briefly the requirements of the software. For the use case model we used the "simple version template" suggested by Scott Ambler~\cite{UCS-Template}
% \textit{ \hl{(el caso que elijamos: ML-JC-OV-MS )}} case study from \cite{Schach:2010}. 
% Its content was designed for the description to occupy less than one page and for its reading to take place in less than five minutes.  
% The model artifacts are a BPMN model and a Use Case Specification. 
% The BPMN models the required process of the case study. This is a TO-BE model, it means the model represents what the case study process would look like if the required software were available. 
% The Use Case Specification is presented in a classic format from \Olga{Traer referencia de consulta}. It describes the requirements of the software needed for solve the problems stated in the case study.  


We have planned an evaluation session with a total of 50 CS/Systems Engineering Master students. Some of them have previous experience on analysis and development, while others have experience on managing software projects. They are knowledgeable of the BPMN notation and the Use Cases specification format.

The participants will be distributed in three groups, each one receiving the same case study and questions, but receiving a different set of the others artifacts: (i) a control group provided with the BPMN model, (ii) a treatment group provided with the Use Case specification,  and (iii) a second treatment group provided with both BPMN model and the Use Case artifact. 
% Table \ref{ArtifactsXGroup} summarizes this delivery.   

Before running the experiment, we will have a pilot study with two volunteers with experience with both BPMN and Use Cases. This pilot study aims to detect situations and problems that may occur during the application of the experiment with the students. In particular, we expect to (i) check the readability of the case study, the BPMN model and Use Case Specification; (ii) review the quality of the questions, and (iii) determine the time needed to apply the experiment.


\begin{comment}

    \begin{table}[!ht]
    \centering
    \caption{Artifacts distribution on sample}
    \label{ArtifactsXGroup}
    \begin{tabular}{@{}cccc@{}}
    \toprule
    \multicolumn{1}{l}{\cellcolor[HTML]{C0C0C0}}                 & \multicolumn{3}{c}{\cellcolor[HTML]{EFEFEF}\textbf{Artifacts}}                                                                             \\ \midrule
    \multicolumn{1}{|c|}{\cellcolor[HTML]{EFEFEF}\textbf{Group}} & \multicolumn{1}{c|}{\textbf{Case Study}} & \multicolumn{1}{c|}{\textbf{BPMN model}} & \multicolumn{1}{c|}{\textbf{Use Case Specification}} \\ \midrule
    \multicolumn{1}{|c|}{\textbf{1}}                             & \multicolumn{1}{c|}{\Checkmark}          & \multicolumn{1}{c|}{\Checkmark}          & \multicolumn{1}{c|}{}                                \\ \midrule
    \multicolumn{1}{|c|}{\textbf{2}}                             & \multicolumn{1}{c|}{\Checkmark}          & \multicolumn{1}{c|}{}                    & \multicolumn{1}{c|}{\Checkmark}                      \\ \midrule
    \multicolumn{1}{|c|}{\textbf{3}}                             & \multicolumn{1}{c|}{\Checkmark}          & \multicolumn{1}{c|}{\Checkmark}          & \multicolumn{1}{c|}{\Checkmark}                      \\ \bottomrule
    \end{tabular}
    \end{table}

\end{comment}

\begin{comment}

    \subsection{Analysis}
    
    The data collected to be analized can be summarized in this way: 
    
    \begin{itemize}
      \item Correctness questions
      \item Time spent
      \item Process carried out
      \item Perception
      \item Background knowledge
    \end{itemize}
    
    We hope that in the data analysis, we could (1) State findings about requirements comprehension when using BPMN and Use Case Specification -with the two first groups of data (2) Formulate relations and construct theories for improving software requirements comprehension, based on the modeling artifacts used for it - Contrasting the findings whit the other groups of data. 

\end{comment}

\endinput
