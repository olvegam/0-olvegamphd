\documentclass[a4paper,twoside]{article}
\usepackage[utf8]{inputenc}  

\usepackage{graphicx}               %% graphics

\usepackage{multicol}               %% multiple columns

\usepackage[inline]{enumitem}       %% inline enumerations
\usepackage{xspace}                 %% space

\usepackage{ifthen}                 %% if.. then
\usepackage{xcolor}                 %% colors

\usepackage{comment}                %% comment blocks

\usepackage{pslatex}                %% tricks used by scitepress template
\usepackage{apalike}                %% APA-like citations
\usepackage{SCITEPRESS}             %% Scitepress template


%% configuration
%% =============

\newboolean{showauthors}
\setboolean{showauthors}{true} % toggle to show or hide authors

\newboolean{showcomments}
\setboolean{showcomments}{true} % toggle to show or hide comments

%% macros for abbreviates
%% ======================

\newcommand{\ie}{i.e.,\xspace}
\newcommand{\eg}{e.g.,\xspace}
\newcommand{\aka}{a.k.a.,\xspace}

%% macros for comments
%% ===================

\newcommand{\nbnote}[2]{
    \ifthenelse{\boolean{showcomments}} {
        \noindent\fcolorbox{blue}{yellow}{\bfseries\sffamily\scriptsize#1}
        {\textsf\small$\blacktriangleright$\textit{#2}$\blacktriangleleft$}
    }{}
}
   
\newcommand\here[1]{\nbnote{HERE}{#1}}     
\newcommand\todo[1]{\nbnote{TO DO}{#1}}

\newcommand\jaime[1]{\nbnote{Jaime}{#1}}
\newcommand\olga[1]{\nbnote{Olga}{#1}}
\newcommand\helga[1]{\nbnote{Helga}{#1}}



\begin{document}

%% Titulo
%% ======

\title{Software Development Process supported by Business Process Modelling
\subtitle{An Experience report} }

\ifthenelse{\boolean{showauthors}} {
    \author{
        \authorname{
            Olga Vega\sup{1},
            Helga Duarte\sup{2}, 
            Jaime Chavarriaga\sup{3}
        }
        \affiliation{
            \sup{1}Universidad de los Llanos, Villavicencio (Meta), Colombia}
        \affiliation{
            \sup{2}Universidad Nacional de Colombia, Bogotá, Colombia}
        \affiliation{
            \sup{3}Universidad de los Andes, Bogotá, Colombia}
        \email{
            olvegam@unillanos.edu.co, 
            hduarte@unal.edu.co, 
            ja.chavarriaga908@uniandes.edu.co
        }
    }
}{
    \author{}
}

%% Abstract
%% ========

\keywords{Software Development Process, Business Process Modeling.}

\abstract{
Understanding the business and how it works can help software engineers to build systems that really meet business goals.
For instance, methods such as the Rational Unified Process (RUP) includes activities to model the business in before eliciting requirements.
However, during our practice of software development in academic and ``real-life" projects, we found problems of using these artifacts with stakeholders.
Here we present our experience integrating Business Process Modeling (BPMN) diagrams to the RUP to improve the elicitation of software requirements.
These diagrams resulted easier to understand by Stakeholders.
This paper discusses the challenges we faced using RUP and how we integrate conceptual maps and BPMN into the process.
In addition, we illustrate the changes using models of a real project implemented using this approach.
}

\onecolumn \maketitle \normalsize \vfill

%% Introduction
%% ============

\section{\uppercase{Introduction}}
\label{sec:introduction}

Nowadays, software is a key element in business strategies and processes.
Very often organizations request new software aiming to improve their business or contribute to their goals.
Understanding the business and how it works can help software engineers to build systems that will really meet the expectations.

In the Universidad de los Llanos (Unillanos)\footnote{The Universidad de los Llanos (Unillanos) is the biggest public university in the eastern plains in Colombia}, we have used RUP in academic and ``real life" software development projects.
It is used not only in courses related to software engineering 
but also in development projects aimed to the university and other affiliated institutions.
Faculty members and students are usually involved in these projects trying to apply the concepts and models described in the theory into real settings.
However, after our experience in several projects between 2010 and 2017, we have integrated  Business Process Models, \ie BPMN diagrams \cite{BPMN}, into the RUP activities to improve the business  understanding by stakeholders and developers.

This paper presents our experience using BPMN diagrams to support activities for eliciting and specifying  software requirements.
Here we present some of the challenges we faces trying to use RUP to understand business processes, and obtain agreements on the corresponding software requirements.
We present our integration of BPMN and RUP and discuss some lessons learned in multiple academic and ``real-life" software development projects.

Rest of this paper is organized as follows: 
Section \ref{sec:challenges} 
describes our experience on several software development projects and how we integrated BPMN in this context.
Section \ref{sec:proposal} presents the modifications we made to RUP to integrate BPMN
and Section \ref{sec:conclusions} concludes the paper. 

%% Context
%% =======

\vspace{-1em}
\section{\uppercase{Eliciting requirements for Business Applications}}
\label{sec:challenges}

Developing software to support business processes implies an understanding of these processes.
For instance, the method we used, the RUP - Rational Unified Process -, includes activities to  model the business processes before eliciting the software requirements.
\vspace{-1em}
\paragraph*{Eliciting requirements using RUP.}

In RUP, business processes are analyzed using diverse artifacts: 
(1) Each process is depicted using a BUCM - Business Use-Case Model,\ie by a circle and a set of relationships to the diverse actors that participate.
(2) The activities in each process are specified using an AD- Activity Diagrams, \ie a flowchart-like model that represents the steps that comprises the process \cite{Jacobson:1999}. 
Finally, (3) Additional information is specified in a glossary and a set of textual descriptions and specifications.

\vspace{-1em}
\paragraph*{Challenges using RUP}

Although there are some books and guides describing how to elicit requirements based on the models representing the processes in RUP, we faced multiple challenges.

\textit{Using the glossary:}
our software engineers had problems to write correctly the terms in the glossary. 
Some times they wrote incorrect definitions or assumed incorrectly that two terms were equivalent. The problems were aggravated by users that refused to read and correct glossaries with a large number of terms.

\textit{Using the BUCMs:}
software engineers and stakeholders had problems to review the BUCMs. These models represent processes as use-cases without specifying an ordering of the tasks, inputs and outputs neither the people that perform the tasks. Many times the stakeholders approved a BUCM because they represented steps in the process without noting missing tasks or missing participants.

\textit{Using the ADs:}
finally, activity diagrams cannot represent some elements important in the processes such as escalations and deadlines. Software engineers and stakeholders were able to use ADs to discuss on the typical case of the process but failed to represent there the exceptional flows.

Additionally, we found problems trying to create a single set of models representing the business processes. 
We faced many projects were the stakeholders were sure of the steps in the current process but not of the steps in the process they want.
In addition, we had to deal with constant changes on the stakeholders, the domain’s experts, the involved legal regulations and/or the software requirements.
Many times, the models combined elements of the current process with one or more of the desired processes.
It was hard, when a change in a regulation or stakeholder occurred, to determine which elements must be modified.
Moreover, some stakeholders did not received well the ideas and recommendations of the software engineers because they believed that the software engineers did not understand the process because these models.
 
\paragraph*{Related Work}

There are other methods and modeling notations aimed to represent business process. 
For instance, the BPMN - Business Process Modeling and Notation - is an OMG standard for ``bridging the gap between process design and implementation".
Although business modeling can be done independently of the software development, it is clear that each process can benefit from the other. 

There are studies that shows a misalignment between the software applications and the business processes of many organizations \cite{Prz14}. These studies describe, as a main reason, the lack of methodology and rigor to determine the software requirements from the business goals. Other studies \cite{SL14} \cite{MAA12} have shown the benefits of determining requirements based on the modeling of the business processes that the software aims to support. 
Thereby, we can find tools integrating business and software modeling  such as IBM Rational and Visual Paradigm. 

%% Proposal 
%% ========== 

\vspace{-1em}
\section{\uppercase{Our Proposal}}
\label{sec:proposal}

To overcome the mentioned problems we integrated conceptual maps and BPMN diagrams into the Requirements activities of the RUP.

\begin{figure}[ht!]
    \centering
    \includegraphics[width= 0.45 \textwidth]{images/SwP-BMReqPhase}
    \caption{Changed Requirements activities.}
    \label{fig:BMReq}
\end{figure}

Figure \ref{fig:BMReq} shows the process starting when a client requests a new software.
Then, the project team model the business and elicit the requirements.
The requirement specifications are later reviewed with the client.
If the client agrees, the process follows to other phases such as analysis, design and implementation (out of the scope of this paper).
 
%% Business Modeling discipline
%% ============================   
    
\subsection{Model business}

The main change we made on the \textit{Business Modeling} sub-process were the artifacts produced, showed in Figure \ref{fig:SwP-modelbus}. We complemented the glossary with a conceptual map, and replaced the BUCM and AD by an AS-IS business process model. In addition, once they are built, the conceptual map and the business process models are reviewed and modified directly with the client.  

\begin{figure}[ht!]
    \centering
    \includegraphics[width= 0.45\textwidth]{images/SwP-ModBus}
    \caption{Model business sub-process.}
    \label{fig:SwP-modelbus}
\end{figure}

\vspace{-1em}
\paragraph*{Conceptual map:}

A \textit{conceptual map} is a technique to graphically represent the knowledge. 
The project team constructs a first version showing the concepts and the relations between them such as they understood. Then, using a whiteboard the map is reviewed and modified with the domain experts. The purpose of this graphical artifact is to facilitate the concepts review between the development team and the client, and, in this way, contribute to an adequate communication in the project, as a result of talking in the same terms.

\paragraph*{BPMN AS-IS:}

The \textit{BPMN AS-IS} represents the current business processes. 
Developers create an initial version of this artifact, showing the processes such as they have understood, then they review and adjust the models with the help of domain experts, using markers on the whiteboard where developers presented the models. 

%% Requirements 
%% =======================

\subsection{Elicit requirements}
% \subsection{BPMN TOBE before elicit requirements }

In our proposal, the \textit{Requirements} sub-process (detailed in Figure \ref{fig:SWP-ER}) starts with the activity of build a \textit{BPMN TO-BE} model representing the intended process, for which the software will be build.

\begin{figure}[ht!]
    \centering
    \includegraphics[width= .45\textwidth]{images/SwP-ElicitReq}
    \caption{Elicit Requirements sub-process}
    \label{fig:SWP-ER}
\end{figure}

When it is approved, the designed process is used to define the scope and the requirements of the software in the showed \textit{Build Req.Artifacts} sub-process.

\paragraph{BPMN TO-BE:}

The \textit{BPMN TO-BE} model represents the intended process. It is created from a first version presented by the development team, which incorporate software to automatize or support users in some of their tasks, then, based on the diverse software alternatives that may improve the process, analyzing the implications on the organization, which technologies can be used, and the time and costs that the development may imply, the stakeholders decide which is the most appropriated. The decisions are registered on a whiteboard where the domain experts make changes directly in the BPMN. 

\paragraph{Other requirement artifacts:}

Once the TO-BE process have been defined, the requirements discipline continues as proposed by RUP.
The software engineers build use cases, scenarios and mockups based on the TO-BE process defined before. 

%% A partir de aquí los Zoom-in here y los Zoom-in there con el ejemplo. 

\subsection{Madiba example}

Here we illustrate the application of our process.

Madiba is a software that support the conciliation process in Colombia. The conciliation is a legal act provided by Colombian law for citizens aimed to resolve conflicts with the help of a neutral third party, before going to court. This is a project widely documented, whose terms are rare and very particular of a legal business domain. After receive from the client whole piles of documents, the software team elaborated a glossary and an initial conceptual map to graphically represent the knowledge that they have achieved so far. In a jointly elaboration with the client, we obtained a concise graphical artifact showing the main domain concepts and their relations; and  consequently we could assure a correct definition of them in the glossary. 

\begin{figure}[ht!]
    \centering
    \includegraphics[width=0.45\textwidth]{images/Madiba-CM}
    \caption{Madiba Conceptual Map fragment.}
    \label{fig:Madiba-CM}
\end{figure}

\paragraph{Conceptual map.} One excerpt of the conceptual map  
is in Figure \ref{fig:Madiba-CM}; it abstracts dozens of documents to show that a \textit{Conciliation Center} facilitates the solution of conflicts by means of a \textit{conciliation} in which a \textit{petitioner}, a \textit{third party} and a \textit{conciliator} intervene in an \textit{audience}. The \textit{conciliator} is designated from an official list managed by the center and may be designated by \textit{shift} or \textit{request}. 

%%=======
\paragraph{Business Process AS-IS.} Once the elaboration of the conceptual map was going on, the software development team presented a first version of the conciliation process AS IS . Then jointly completed with the client, using the whiteboard, we obtained a BPMN modeling the conciliation. Figure \ref{fig:Madiba-ASIS} is an excerpt showing the conciliator designation by shift. 
   
\begin{figure}[ht!]
    \centering
    \includegraphics[width= 0.45\textwidth]{images/Madiba-BM-DC-ASIS}
    \caption{Madiba BPMN AS-IS, Request phase fragment}
    \label{fig:Madiba-ASIS}
\end{figure}

After review the kind of designation, if it was by shift, the top lawyer on the list must be moved to the end, next, validate if he is enabled and update the request with him. By the contrary, the lawyer must be removed from the official list,  take note to call him after, and repeat the cycle until get an enabled lawyer. If the conciliator was selected by the petitioner, after removed from the request is updated. The whole business process was elaborated jointly with the client 
using the whiteboard with markers on it. This guaranteed representing the business in the way and the terms the client do and use. 

%%=======
\paragraph{Business Process TO-BE.} 
Once approved the \textit{AS-IS Process} model the development team transformed it into a \textit{TO-BE Process}  incorporating the software to be build. As a result, the request phase fragment, showed up, is transformed to a simple software supported task: \textit{designate conciliator} in Figure \ref{fig:Madiba-BP-DC-TOBE} that shows an excerpt of the TO-BE process for the Madiba project. 

\begin{figure}[ht!]
    \centering
    \includegraphics[width=.42\textwidth]{images/Madiba-BP-DC-TOBE}
    \caption{Madiba BPMN TO-BE, Request phase cut}
    \label{fig:Madiba-BP-DC-TOBE}
\end{figure}

Again, this activity was realized jointly with the client (the lawyer group), and as a result, before elicit requirements, it was possible to take decisions that involve organizational, technical, financial and time issues. As an example, the next activity after designate conciliator is to announce that to the lawyer. In this example, the client decided to use the major available technologies (i.e. SMS, e-mail and whatsapp messages) to realize this ad in the way showed in Figure \ref{fig:Madiba-DAd-TOBE}
 
The \textit{conciliator} receives a message asking her/him be a conciliator for a case (\ie the \textit{designation} message).
He can accept by sending his availability (\ie the \textit{schedule availability} task), or can decline by sending a justification (\ie the \textit{decline} task). The resulting requirements specifications were faithful to the need and decisions made by the client, and they are easily verifiable in the TO-BE process.  

\begin{figure}[ht!]
    \centering
    \includegraphics[width=.45\textwidth]{images/Madiba-BP-DAd-TOBE}
    \caption{Madiba BPMN TO-BE, Request phase cut}
    \label{fig:Madiba-DAd-TOBE}
\end{figure}


%% Conclusions and future work
%% ==========================

\vspace{-1em}
\section{\uppercase{Conclusions}}
\label{sec:conclusions}

We found that BPMN help us to elicit the requirements, because the stakeholders were able to discuss and propose changes to the artifacts, to the scope and the requirements of the software.

Our current and future work is focused on \begin{enumerate*}[label=(\arabic*)] \item to implement traceability between the high level artifacts depicted here towards the code, to facilitate the software evolution and requirements change management. \item  Research on ways to manage BPMN artifacts (linked with use cases and code) as software assets with which a software product line strategy can be implemented. 
\end{enumerate*}

%% Bibliography
%% ============

\bibliographystyle{apalike}     %% APA
{\small
\bibliography{bibliography/bibliography}}

\end{document}

