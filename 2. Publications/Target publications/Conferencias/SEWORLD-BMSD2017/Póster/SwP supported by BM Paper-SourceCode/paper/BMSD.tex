\documentclass[a4paper,twoside]{article}
\usepackage[utf8]{inputenc}  

\usepackage{graphicx}               %% graphics

\usepackage{multicol}               %% multiple columns

\usepackage[inline]{enumitem}       %% inline enumerations
\usepackage{xspace}                 %% space

\usepackage{ifthen}                 %% if.. then
\usepackage{xcolor}                 %% colors

\usepackage{comment}                %% comment blocks

\usepackage{pslatex}                %% tricks used by scitepress template
\usepackage{apalike}                %% APA-like citations
\usepackage{SCITEPRESS}             %% Scitepress template


%% configuration
%% =============

\newboolean{showauthors}
\setboolean{showauthors}{false} % toggle to show or hide authors

\newboolean{showcomments}
\setboolean{showcomments}{true} % toggle to show or hide comments

%% macros for abbreviates
%% ======================

\newcommand{\ie}{i.e.,\xspace}
\newcommand{\eg}{e.g.,\xspace}
\newcommand{\aka}{a.k.a.,\xspace}

%% macros for comments
%% ===================

\newcommand{\nbnote}[2]{
    \ifthenelse{\boolean{showcomments}} {
        \noindent\fcolorbox{blue}{yellow}{\bfseries\sffamily\scriptsize#1}
        {\textsf\small$\blacktriangleright$\textit{#2}$\blacktriangleleft$}
    }{}
}
   
\newcommand\here[1]{\nbnote{HERE}{#1}}     
\newcommand\todo[1]{\nbnote{TO DO}{#1}}

\newcommand\jaime[1]{\nbnote{Jaime}{#1}}
\newcommand\olga[1]{\nbnote{Olga}{#1}}
\newcommand\helga[1]{\nbnote{Helga}{#1}}



\begin{document}

%% Titulo
%% ======

\title{Software Development Process supported by Business Process Modelling
\subtitle{Experiencies and perspectives} }

\ifthenelse{\boolean{showauthors}} {
    \author{
        \authorname{
            Olga Vega\sup{1},
            Helga Duarte\sup{2}, 
            Jaime Chavarriaga\sup{3}
        }
        \affiliation{
            \sup{1}Universidad de los Llanos, Villavicencio (Meta), Colombia}
        \affiliation{
            \sup{2}Universidad Nacional de Colombia, Bogotá, Colombia}
        \affiliation{
            \sup{3}Universidad de los Andes, Bogotá, Colombia}
        \email{
            olvegam@unillanos.edu.co, 
            hduarte@unal.edu.co, 
            ja.chavarriaga908@uniandes.edu.co
        }
    }
}{
    \author{}
}

%% Abstract
%% ========

\keywords{Software Development Process, Business Process Modeling.}

\abstract{
Understanding the business and how it works can help software engineers to build systems that really meet business goals.
Methods such as the Rational Unified Process (RUP) includes activities to model the business in addition to analyze requirements and design the software.
Business Use Cases and Activity Diagrams are used to represent the business processes.
However, during our practice of software development in academic and ``real-life" projects in the Universidad de los Llanos (Unillanos) between 2010 and 2017, we found problems of using these artifacts with stakeholders.
Instead of using these models, we customize RUP to integrate Business Process Modeling (BPMN) diagrams for the business modeling activities.
These diagrams resulted easier to understand by Stakeholders.
Furthermore, they were able to discuss and propose changes on them.
This describes the challenges we faced using RUP for business modeling, our customization that integrates BPMN into the RUP disciplines and a set of lessons learned in our experience.
}

\onecolumn \maketitle \normalsize \vfill

%% Introduction
%% ============

\section{\uppercase{Introduction}}
\label{sec:introduction}

Nowadays, software is a key element in business strategies and processes.
Very often organizations request new software aiming to improve their business or contribute to their goals.
Understanding the business and how it works can help software engineers to build systems that will really meet the expectations.
Methods such as the Rational Unified Process (RUP) considers these contexts and includes activities to model the business in addition to the software.

In the Universidad de los Llanos (Unillanos), we have used RUP in academic and ``real life" software development projects.
It is used not only in diverse courses related to software engineering in undergraduate and postgraduate courses, but also in development projects aimed to the university and other affiliated institutions.
Faculty members and students are usually involved in all these projects trying to apply the concepts and models described in the theory into real settings and projects.
However, after our experience in several projects between 2010 and 2017, we decided to customize RUP and introduce Business Process Models and Notation (BPMN) diagrams to improve the activities for business understanding.

In RUP, business modeling is performed by using Business Use Case Models and Activity Diagrams.
However, in many projects, the stakeholder failed to understand these models and provide useful feedback on the artifacts produced in the project.
Instead of using these models, we customize RUP to model the business using BPMN diagrams.
These models resulted easier to understand and modify by the stakeholders.

In addition, we defined activities where stakeholders and software engineers use whiteboards to review, discuss and propose changes on these process models.
There, they create models for the current process, \ie the AS-IS process, and the intended process, \ie the TO-BE process.
Later, these models are used to elicit and specify the requirements for the software that will support the process.



Rest of this paper is organized as follows: 
Section \ref{sec:context} describes the context which the experience was developed.
Section \ref{sec:background} presents an overview of RUP, the modelling of requirements in RUP, and the modelling of business processes using BPMN.
Section \ref{sec:challenges} describes our experience on several software development projects and the challenges we faced.
Section \ref{sec:proposal} presents the modifications we made to RUP to integrate BPMN.
Section \ref{sec:evaluation} summarizes our lessons learned,
and Section \ref{sec:conclusions} concludes the paper. 

%% Context
%% =======

\section{\uppercase{Context: Universidad de los Llanos}}
\label{sec:context}

In this paper, we described our experience on multiple academic and ``real-life" software development projects in the Universidad de los Llanos.

The \textit{Universidad de los Llanos (Unillanos)} is the biggest public university in the eastern plains in Colombia.
Its Engineering Faculty offers multiple academic programs related to computer science and software engineering.
For instance, there is an undergraduate program in Systems Engineering and a postgraduate one in Software Engineering. 

\paragraph{Software Development in Academic projects:}
The students of the programs in Systems and in Software Engineering must develop software as part of some of their courses.
Courses such as \textit{Software Engineering foundations}, \textit{Software Project Management} and \textit{Software Modeling} imply final projects where a software product must be created. Usually, each project is performed by a team of five students. In average, there are five to six group-projects each semester per each course. 

The software to develop as final project is typically defined by the students themselves. 
For instance, in the last years we can mention projects in a rich variety of domains: one for managing the work delegated to the lawyers in a buffet, other for electronic voting at Unillanos, another for managing the musicians of a records company, 
a social network for sharing books, an enrollment system for the language institute and another one for managing the clinical laboratory of the University.

\paragraph{Software Development for ``real-life" projects:} \label{real-life}
In addition to the academic projects the Engineering Faculty has been involved in many projects for developing ``real-life" applications since 2011.
At that time, the processes of the University was supported by a legacy application.
The CiENTiC academic unit was created to support the development of new solutions for the University with the participation of professors, faculty members and hired professional software engineers.

Among the most relevant real-life projects, we can mention: \textit{ConD}, a software for managing the merit contest required in the Unillanos to be professor; \textit{GAVi}, a generator for Moodle virtual classroom; 
\textit{Huellas}, a system to manage the curriculum of professors and faculty members; 
\textit{SARA}, a software for assigning academic responsibilities; 
\textit{EVa}, a system for evaluating professors;
\textit{Madiba}, a software to support conciliation as a legal instrument for conflict resolution in Colombia; \textit{CarSharing}, a platform to share transport from and to Unillanos and the Villavicencio city;
\textit{Bioflocos} and \textit{SenVara}, platforms to analyze physical and environmental conditions obtained by Wireless sensor networks (WSN) for aquaculture and agriculture; and
\textit{MAc}, a system to manage International Mobility of Academics. 
   

%% Background
%% ==========

\section{\uppercase{Concepts of Software Development Processes}}
\label{sec:background}

Both types of software development projects (\ie academic and real-life projects) have been highly influenced by the methods and modeling notations presented in the academic courses in Unillanos.
Since 2010, the software engineering courses have been based on the Rational Unified Process (RUP).
The courses for organizational systems have been based on the Business Process Modeling and Notation (BPMN).
This section presents an overview of both. 

%% RUP
%% ===

\subsection{Rational Unified Process}

The \textit{Rational Unified Process (RUP)} is a customizable software development process framework widely adopted by companies around the world since their introduction at the nineties~\cite{Jacobson:1999}.

RUP combines two perspectives: a \textit{technical perspective} that considers the different disciplines (workflows) that must be accomplished to develop the software, and an \textit{administrative perspective} that defines a set of project phases and iterations aimed to produce small improvements to the general product incrementally~\cite{Kruchten:1996}. 


%% Business Modeling
%% =================

\subsection{Business Modelling and Requirements in RUP}

In RUP, the \textit{Business Modeling discipline} comprises activities for understanding the business processes by  identifying the business workers, their responsibilities and the tasks they perform.
These tasks are supported by three types of artifacts: 
The Business Use-Case Model (BUCM) used to represent business processes at strategic and tactical levels, the Business Object Models (BOM) used to describe the realization of these business processes, and the Activity Diagrams (AD) used to represent operational workflows~\cite{IBM-BM}.

The \textit{Requirements discipline} comprises activities to elicit and specify  the required capabilities of the system and the requirements for the software to build. 
The resulting artifacts include a Vision statement, a Glossary defining important terms in the project, a Use Case Model (UCM) representing the intended functions of the system, specifications for each Use Case (UC), and prototypes of the User-Interface.
Usually, all these artifacts are included in a Software Requirements Specification (SRS) document that captures the complete software requirements~\cite{Jacobson:1999}.

Here we must mention that the business modeling can be omitted in RUP.
Some guidelines and RUP variations such as the proposed by Schach~\cite{Schach:2010} start with the software requirements.


%% Business Process
%% ================

\subsection{Business Processes Model and Notation}

The \textit{Business Process Model and Notation (BPMN)} is aimed to ``bridge the gap between process design and implementation” \cite{BPMN}. It provides a standard notation that can be understood by Business Owners who manage and monitor the processes, Business Analysts who specify and design the processes, and Business Developers who implements the process in software systems.

In BPMN, a \textit{Business Process Diagram (BPD)} is a specialized flow-charting technique used to create graphical models of the business processes.
There are many specialized tools for creating and analysing BPD models. An analyst can use these tools, for instance, to detect errors in a diagram, to detect conflicts among diagrams and to simulate the behaviour of the process represented there.

\subsection{BPMN and Software Requirements}

Although business modeling can be done independently of the software development, it is clear that each process can benefit from the other.

There are studies that shows a misalignment between the software applications and the business processes of many organizations~\cite{Prz14}. 
These studies describe, as a main reason, the lack of methodology and rigor to determine the software requirements from the business goals. 
Other studies~\cite{SL14}\cite{MAA12} have shown the benefits of determining requirements based on the modeling of the business processes the software aims to support. 
% Therefore, we are facing a  vicious circle, where the specification of requirements for software applications that support business processes must be done, precisely, with a business process-oriented approach.

There are proposals integrating business modeling and software requirements.
For instance, IBM and Visual Paradigm integrate BPMN Diagrams with UML UCs~\cite{IBM-BM-UC-Doc}.
They allow modelers to identify the use cases in a business process diagram
The software provides traceability and navigation links from BP elements to UCs, and backwards.
However, they do not define an approach where the models for processes and requirements are created together.  

% https://www.visual-paradigm.com/tutorials/from-business-process-to-use-cases.jsp



%% Challenges 
%% ========== 

\section{\uppercase{Challenges using RUP}}
\label{sec:challenges}
    
Developing the mentioned projects, we faced several challenges on using RUP. % without modeling the corresponding processes. 
This section describes these challenges and some customizations we made to integrate BPMN in the activities.

\subsection{Understanding the Business domain}

In RUP, a \textit{Glossary} is used to acquire familiarity with the client domain and to use correct terminology when communicating with the client and potential users of the software~\cite{Jacobson:1999}\cite{Schach:2010}.

In our experience, we identified some problems using this glossary:
First, the software engineers had problems to write correct and easy to understand definitions for the business terms.
Second, these definitions were not well reviewed by the domain experts.
Sometimes, although the experts approved the glossary, they later found problems and errors in the definitions.
And Finally, despite all the effort invested in the creation of this artifact, sometimes the people involved in the process used their own words instead of using the terms included in the glossary.

Instead of using the glossary, we look for an artifact that can be defined and reviewed jointly by engineers and experts.
We used \textit{conceptual maps} that illustrated in a graphic and synthetic way the concepts in the domain and their relations. 
Instead of tasks where the experts approved the terms, we defined activities where engineers and experts reviewed the map and introduced new elements to it in the process.

The benefits obtained from using a graphical model that can be defined jointly by software engineers and domain experts motivated us to continue searching this kind of strategies to achieve a fluid communication in the other activities. 

\subsection{Understanding the Business Processes} 

As mentioned before, in RUP business processes are analyzed using diverse models: each process is depicted using a BUCM and the activities in the process using an AD~\cite{Jacobson:1999}. % \cite{Schach:2010}. 

We had identified some difficulties on using these artifacts:
On the one hand, the domain experts may approve the artifacts without a complete understanding of them.
Sometimes, we found experts that approved the specifications of the processes and later described verbally the process including tasks and validations no included in the artifact without noting the difference.
On the other hand, software engineers tend to mix in the artifacts the current and the expected system.
Sometimes it was hard to determine if the artifact described the process before the software we are building or if it described the changes proposed to improve the process with the new software.
Other times it was hard to determine if the artifacts described use cases that support the business process or additional requirements defined by the engineers not directly related to business activities.

Instead of UCs and ADs, we used BPMN models to describe the business processes independently of the software requirements. 
For instance, we used a BPMN model to describe the current process before the software, \ie the AS-IS process.


\subsection{Understanding Process Relationships}

Business processes may be related: \eg it is possible that a process starts another one, or that many processes cooperate to produce an output.
In RUP, these interactions are represented using ADs~\cite{IBM-BM}.

When we tried to use AD to specify interactions among processes we found several issues:
One the one hand, the diagram is mainly focused on control flow and do not include many elements to represent the messages and triggers that may start another process neither the intermediate products that may be produced.
Other elements such as timers and scalations cannot be represented at all.
On the other hand, when some processed performed by external companies, it is hard to describe in the AD which steps are performed internally, which are performed manually and which others are performed by external actors.

Instead of the ADs, we use the BPMN elements to discuss with domain experts the interactions among the processes and the scope of the software to build. 


\subsection{Eliciting Requirements}

A UCM describes the intended functions of a system. 
Each function is depicted as a UC.
In turn, each UC must be described in detail using an additional specification with the sequence of actions of the system and the involved actors (\ie users).

In our experience, it was not obvious for the user which use cases must be implemented to support a process. 
When we tried to identify the requirements from the current process (\ie the as-is process), experts and engineers may define different sets of use cases.
Both may have different ideas about how the software may be used to support the process.

Instead of trying to identify the use cases from the start, we defined an step where domain experts and engineers design the new process.
Here, they can agree on how the software can support the process and which tasks will remain manual.
This step aims to describe the intended business process, \ie the TO-BE process.
The use cases are determined later based on this process.

\subsection{Defining User Roles.} 

In RUP, the user roles in the application are described as actors in the BUCs and UCs.
They may be also included as columns in the ADs. 

We had problems defining the user roles from the diverse artifacts (BUCs, UCs and ADs) where the actors appear:
Basically, it was hard to maintain the models and  determine inconsistencies among them.
On the one hand, if we were interested in modifying a user role, \eg to give them more responsibilities, we need to look for all the instances of that actor and modify the models accordingly.
On the other hand, if some model used a different actor name it was hard to determine which is the correct.

Instead of using information scattered on multiple models, we use the definitions in the BPMN to determine user roles.
In BPMN, the activities are assigned to pools and swimlanes that represent the participants in the process.
We used that information to define the user roles of the software.


\subsection{Managing changes}

Finally, in almost all the projects we faced, we had to deal with changes on the stakeholders, the domain experts, the involved legal regulations and/or the software requirements.

In RUP, any change implies modifications in multiple artifacts: 
\eg a change on a requirement may imply changes in the UCM, in a UC, in the corresponding ADs, and so on.
A Traceability Matrix can be used to specify the relationships among the artifacts and determine the required modifications when an artifact changes.
However, in our experience, it was hard to keep a trace of all the changes.
Sometimes we ended with sets a of outdated and/or inconsistent models and artifacts.

Instead of defining only traceability links to requirements and user interviews, we managed the multiple versions of the TO-BE processes and keep a trace among their elements, the use cases and the other artifacts we build in the process.
Any change in the scope or in the requirements of the project were reflected first in the BPMN.
This helped us to determine the impact of the changes and to approve them when they were appropriated.
Once approved, the changes were then propagated to the other artifacts.


%% Proposal 
%% ========== 

\section{\uppercase{Our Proposal}}
\label{sec:proposal}
We customized RUP to tackle the challenges we mentioned before.
Basically, we integrated BPMN to support the business modeling and requirements disciplines.

\begin{figure}[ht!]
    \centering
    \includegraphics[width= 0.45 \textwidth]{images/SwP-BMReqPhase}
    \caption{Changed Requirements phase.}
    \label{fig:Req}
\end{figure}

Figure \ref{fig:Req} shows the modified disciplines using a BPMN diagram.
The process starts when a client or a stakeholder requests a new software.
Then, the project team model the business and elicit the requirements.
The requirement specifications are later reviewed with the client.
If the client agrees, the process follows with other engineering disciplines such as analysis, design and implementation (out of the scope of this paper).
 
      
%% Business Modeling discipline
%% ============================   
    
\subsection{Business Modeling}

The main change we made on the \textit{Business Modeling} discipline were the artifacts produced. 

\begin{figure}[ht!]
    \centering
    \includegraphics[width= 0.45\textwidth]{images/SwP-ModBuss}
    \caption{Model business sub-process.}
    \label{fig:SwP-modelbuss}
\end{figure}

Figure \ref{fig:SwP-modelbuss} shows an overview of our business modeling: 
We complemented the glossary with a conceptual map, and replaced the BUCM and AD by an AS-IS process model.
In addition, once they are built, the conceptual map and the business process models are reviewed and modified directly with the client.    


\paragraph*{Conceptual map:}

A \textit{conceptual map} is a diagram that depicts concepts and relationships among them.
We decide to use it because these maps can be understood and modified directly by the experts.
In contrast to the glossary, with these maps we can assure that the definitions therein can been reviewed completely by the clients. 

\begin{figure}[ht!]
    \centering
    \includegraphics[width=0.45\textwidth]{images/Madiba-CM-part}
    \caption{Madiba Conceptual Map cut.}
    \label{fig:conc-map-ex}
\end{figure}

Figure \ref{fig:conc-map-ex} shows an example conceptual map of the Madiba project:
There is a \textit{conciliation process} that starts with a \textit{request} and where intervene a \textit{conciliator}, a \textit{petitioner} and a \textit{third party} summoned to an audience.
In the map, each box denotes a concept and each line a relationship between two concepts.

In our approach, concepts and associations be included in both artifacts: the glossary and the conceptual map. 
The concept maps are mainly used to discuss and precise the concepts.
Later, these maps are used to review and verify the glossary. 


\paragraph*{AS-IS process:}

The \textit{AS-IS process model} represents the current business processes. 
Although developers create an initial version of this artifact, they review and adjust the models with the help of domain experts.
In our projects, we used whiteboards where developers can present the models and experts may propose changes directly using markers on it. 

\begin{figure}[ht!]
    \centering
    \includegraphics[width= 0.45\textwidth]{images/Madiba-ASIS-CA}
    \caption{Madiba BPMN AS-IS, Request phase cut}
    \label{fig:Madiba-ASIS}
\end{figure}

Figure \ref{fig:Madiba-ASIS} shows an excerpt of the AS-IS process in the Madiba project.
Here, the process describe how, when a conciliator (\ie a lawyer) is designated to a case, she/he must answer before a deadline.
The deadline is represented using a timer and the response using a message.
If the conciliator does not accept the case, the designation must be done again; otherwise, an audience is scheduled.
% As an example, in the Madiba project, one piece of interest of its BPMN ASIS is the conciliator designation after which two events can happen: the deadline for receiving an answer expires or the conciliator answer the designation. 
% The BPMN portion on figure \ref{fig:Madiba-ASIS} shows the possible flows: the process follows to the next phase or the conciliator designation must be repeated.  

%% Requirements discipline
%% =======================

\subsection{Requirements}
% \subsection{BPMN TOBE before elicit requirements }

In our proposal, the \textit{Requirements} discipline starts by modeling the inteded process.
%We propose begin this sub-process modeling the business (see Figure  \ref{fig:SWP-ER}) as it should function with the system. 

\begin{figure}[ht!]
    \centering
    \includegraphics[width= .45\textwidth]{images/SwP-Req-Elicit}
    \caption{Elicit Requirements sub-process}
    \label{fig:SWP-ER}
\end{figure}

Figure~\ref{fig:SWP-ER} shows an overview of the Requirements discipline:
we specify first a \textit{TO-BE process} describing the process using the software to build. 
This model improves the \textit{AS-IS process} by using software to automatize or support users in some of their tasks.
Then, this new process is reviewed by the domain experts. 
Later, if it is approved, the process is used to define the scope and the requirements of the software.


\paragraph{TO-BE process:}

The \textit{TO-BE process models represent} the intended process. 
It is created jointly by stakeholders and software engineers.
Basically, the engineers present diverse software alternatives that may support the process and the stakeholders decide which is the most appropriated. 
During the process, the stakeholders can analyze the implications on the organization, which technologies can be used, and the time and costs that the development may imply. 

\begin{figure}[ht!]
    \centering
    \includegraphics[width=.45\textwidth]{images/Madiba-TOBE-CA}
    \caption{Madiba BPMN TO-BE, Request phase cut}
    \label{fig:Madiba-TOBE}
\end{figure}


Figure \ref{fig:Madiba-TOBE} shows an excerpt of the TO-BE process for the Madiba project. 
In the BMPMN diagram, the \textit{conciliator} receives a message asking her/him be a conciliator for an specific case (\ie the \textit{designación} message).
She/he can accept it by sending his availability agenda (\ie the \textit{agendar disponiblidad} task), or can decline by sending a justification (\ie the \textit{declinar} task).


\paragraph{Other requirement artifacts:}

Once the TO-BE process have been defined, the requirements discipline continues as proposed by RUP.
The software engineers build use cases, realization models and class diagrams based on the TO-BE process defined before. 



%% Evaluation (Learned Lessons)
%% ============

\section{\uppercase{Lessons Learned}}
% \section{\uppercase{Evaluation} - Learned Lessons}
\label{sec:evaluation}

Our experience developing software using RUP and using our customizations that integrate BPMN can be summarized in the following lessons learned:

%% BPMN-- TO-BE process
\subsection{Business Modelling and Requirements}

%% 1- TO-BE process
\noindent 
\textit{Modeling the TO-BE process helps to elicit the requirements.}

We used the BPMN to discuss with stakeholders on the scope and functionalities of the software to build.
This improved our practice of eliciting the requirements. 
On the one hand, focusing on the new process,  users and experts felt that the discussions were in terms they understand. 
They were able to describe the improvements to the their tasks and the requirements for the software they want.
On the other hand, software engineers were able to understand the business contexts and obtain feedback on the different technical solutions they proposed.

For instance, using BPMN improved our ability to identify correctly the user roles in the software.
In our experience,  stakeholders were able to visualize easily which users do which tasks. 
They discussed and proposed more changes on this subject when using BPMN than when using UCs.
As other example, BPMN improved the involvement of the stakeholders in the definition of the software scope and requirements.
Using a whiteboard, stakeholders often discussed multiple solutions in order to select which one is the most appropriated and must be implemented.

As pointed by other authors~\cite{SL14}\cite{MAA12}, we consider that stakeholders understand more of the BPMN diagrams than of the BUCMs and ADs with less effort.
Using BPMN improved the participation of stakeholders and the communication among all the team members.

%% 2- engineers as advisers
\vspace{1em}
\noindent 
\textit{Software engineers may act as technology advisers during a process improvement.}

During the business modeling and requirements, software engineers acted as advisers that presented diverse technical alternatives for improving the processes.

Basically, during the review of the AS-IS process and the design of the TO-BE process, engineers presented multiple options to implement some tasks and improve the process.
This gave us many benefits:
First, these activities gave more credibility to the project team.
Stakeholders considered that the engineers really understand the business and may provide solutions to the organization.
Second, by considering multiple alternatives, stakeholders and engineers got more confidence on the proposed solution.
And finally, these activities gave us the opportunity to evaluate the alternatives early.
For instance, in some of the projects we did, several options were discarded when they were proposed and others only after a further evaluation.
Discarding options early help us to reduce the time and cost of the design activities.

%% 3. legal requirements
\vspace{1em}
\noindent
\textit{BPMN is very useful in domains where legal requirements and procedures are involved.}

Many of the projects we developed were in public universities and organizations, \ie projects were multiple laws, regulations and legal aspects must be considered.
In such environments, a clear understanding of the legal procedures and concepts is key.
When we used RUP without BPMN we got several problems trying to understand these legal aspects and propose a valid software solution.
After integrating the conceptual maps and BPMN diagrams we note improvements in the comprehension of these aspects by all the team.
For instance, some discussions on the TO-BE processes were focused on the legal implications or the regulation compliance of the changes.



%% Artifacts
\subsection{Artifacts and Project Communication}

%% 4- improve communications
\noindent
\textit{Some artifacts may improve communication among stakeholders and engineers.}

We consider that many of the benefits obtained by using Conceptual Maps and BPMN diagrams are caused by the improvements they offered to the communication among stakeholders and engineers.
Stakeholders and domain experts found UCs and ADs hard to understand.
Even when they believed that understood them, sometimes they found errors or presented inconsistent information after they reviewed and approved these artifacts.
In contrast, they found the conceptual maps and BPMN diagrams easier to understand and were able to discuss and propose changes.

%% 5- user feedback
\vspace{1em}
\noindent
\textit{BPMN help us to obtain feedback from Stakeholders.}

As mentioned before, we defined activities where stakeholders review the process and propose changes in a whiteboard.
This kind of feedback was not possible when we used UCs and ACs.
Although there are some advanced concepts in the BPMN, stakeholders were able to propose changes using the most simple elements.
Even, during the process, they learned more and starts to propose the inclusion of advanced elements such as timers and scalations.
We consider that using these artifacts that the users understand improved the participation of stakeholders and domain experts in the review and design of requirements and solutions.


%% Impact on other disciplines
\subsection{Impact on other processes}

\noindent
\textit{Using BPMN can be used to improve other RUP disciplines.}

Although we had presented our experience improving the business modeling and requirements disciplines, we have found benefits of using BPMN on other disciplines too.
First, BPMNs presents more information about the sequence of the tasks and the integration among multiple processes. 
We used this information to define more complete system-wide and integration test cases.
Second, the information of the processes can be used to review the user-interface an the software designs too.
In our projects, we defined checklists where designs obtained in the other disciplines are reviewed against the TO-BE processes.
Finally, as we mentioned before, we used the BPMN to support change management.
In our projects, traceability of the software artifacts include links to the process elements.
We used the processes and the changes in the processes to guide activities to evaluate the impact of the changes and approve or disapprove user requests.

We must mention that the business modeling improved not only the software development activities but also the organziation processes themselves.
in projects such as EVA, Huellas and COND, the involved processes were modelled by first time.
Stakeholders noted the benefits of modeling the processes and started to use BPMN to model aspects of the business and support knowledge management initiatives.
In these projects, we consider that our modeling efforts introduced to the stakeholders the benefits of modeling processes and the BPM technologies. 


%% Theory and Practice
\subsection{Relationship between theory and practice}

\noindent
\textit{RUP customization may be a good practice.}

Since their beginnings, RUP has been proposed as a process that must be customized according to the company processes and expectations.
However, almost all the literature focus on changing the process activities.
RUP variants such as EssentialUP and AgileUP focus on removing activities and artifacts instead of introducing non-UML models.
In addition, very few present evidence of the benefits of customize the RUP.
We consider that the current trend is the use of other ``agile methods" that use fewer (or none) artifacts, instead of customizing RUP.
Our experiences goes in a different way.
Companies and organization can customize a process such as RUP by including artifacts that all the parties involved can review and propose changes to the artifacts.
This may be specially useful in contexts such public universities and organizations where multiple legal aspects must be considered and we want to keep a track of the changes of the processes and their implications.
In addition, this may be useful to develop enterprise applications which support multiple business processes and which documentation is key to create new applications.

\vspace{1em}
\noindent
\textit{Curriculum can be enriched by experiences in real-life development projects.}

Finally, as we mentioned at the beginning of the paper, here we are presenting our experience in academic and ``real-life projects".
In Unillanos, faculty members, students and system engineers participate in both types of projects.
We have created a synergy where the experiences in the development projects have been used to enrich the curriculum in the undergraduate and postgraduate programs.
Currently, the courses related to software and process engineering rely on our customized RUP process.
We consider that our experience have not only improved the software development process but also the academic courses in the university. 



%% Conclusions and future work
%% ==========================


\section{\uppercase{Conclusions}}
%\section{\uppercase{Conclusions \& Future Work.}}
\label{sec:conclusions}

In this paper, we reported the experience of Unillanos integrating BPMN to the business modeling and requirements disciplines of the RUP process.
Instead of using \textit{Business Use Cases} and \textit{Activity Diagrams}, we have used \textit{Business Process Diagrams} to represent the process to improve,\ie the \textit{AS-IS process}, and the intended process,\ie  the \textit{TO-BE process}.
The new process is used to define the scope and elicit the requirements of the software to build.
In addition, we have introduced the use of \textit{Conceptual Maps} in addition to the Glossary proposed by RUP and we have defined additional activities where stakeholders and engineers review and modify the diverse artifacts.

Our findings are summarized as a set of lessons learned.
Among others, we found that BPMN help us to elicit the requirements.
Because the stakeholders understood these diagrams, they were able to discuss and propose changes to the artifacts, to the scope and the requirements of the software.
In addition, we found that using BPMN improved other disciplines too.
For instance, we used the information of these models to improve the tests, the design reviews and the change management activities.
Furthermore, we found that our experience, combining academic and ``real-life" projects helped us to improve not only the quality of the software but also the courses related to software and process engineering in the university.

This paper is focused on the changes we made to the business modeling and requirements disciplines only. 
Our current and future work is focused on improving the process in other areas:
We are working on
\begin{enumerate*}[label=(\arabic*)]
    \item supporting our process by extending existing opensource modeling solutions such as Eclipse Papyrus, %  or Capella, to allow engineers derive semi-automatically the use cases and the actors from the BPMN diagrams
    \item integrating to the process code-generation tools and/or BPM platforms to produce the corresponding solutions with less effort,  
    \item providing support for configurable processes (or families of processes) that can be defined to develop similar solutions to groups of clients, and
    \item improving other process support disciplines such as configuration management and metric-based process improvement.
\end{enumerate*}


%% Acknowledgments
%% ===============

% \section*{\uppercase{Acknowledgements}}
% \noindent If any, should be placed before the references section
% without numbering.


%% Bibliography
%% ============

\bibliographystyle{apalike}     %% APA
{\small
\bibliography{bibliography/bibliography}}

\end{document}

