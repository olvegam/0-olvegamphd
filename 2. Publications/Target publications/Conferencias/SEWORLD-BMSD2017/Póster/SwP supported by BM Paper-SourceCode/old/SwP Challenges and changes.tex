\documentclass[a4paper,twoside]{article}
\usepackage[utf8]{inputenc}  

\usepackage{epsfig}
\usepackage{subfigure}
\usepackage{float}

\usepackage{calc}
\usepackage{amssymb}
\usepackage{amstext}
\usepackage{amsmath}
\usepackage{amsthm}
\usepackage{multicol}
\usepackage{pslatex}
\usepackage{apalike}

\usepackage{enumitem}
\usepackage{xspace}                         %% xspace

\newcommand{\ie}{i.e.,\xspace}
\newcommand{\eg}{e.g.,\xspace}
\newcommand{\aka}{a.k.a.,\xspace}

\usepackage{SCITEPRESS}     % Please add other packages that you may need BEFORE the SCITEPRESS.sty package.

\subfigtopskip=0pt
\subfigcapskip=0pt
\subfigbottomskip=0pt

\begin{document}

%% Titulo
%% ======

\title{Software Development Process supported by Business Process Modelling
\subtitle{Experiencies and perspectives} }

\author{
    \authorname{
        Olga Vega\sup{1},
        Helga Duarte\sup{2}, 
        Jaime Chavarriaga\sup{3}
    }
    \affiliation{
        \sup{1}Universidad de los Llanos, Villavicencio (Meta), Colombia}
    \affiliation{
        \sup{2}Universidad Nacional de Colombia, Bogotá, Colombia}
    \affiliation{
        \sup{3}Universidad de los Andes, Bogotá, Colombia}
    \email{
        olvegam@unillanos.edu.co, 
        hduarte@unal.edu.co, 
        ja.chavarriaga908@uniandes.edu.co
    }
}

%% Abstract
%% ========

\keywords{Software Development Process, Business Process Modeling.}

\abstract{As a result of the software development practice in the Universidad de los Llanos – Unillanos University, since six years ago, some activities and artifacts of the process have been transformed from the traditional RUP approach to one tailored to overcome some difficulties  faced. This work describes the current process and figure out successes and challenges. The experience is based on semester projects in undergraduate courses and real-life software development projects where several needs of improvement were found. Finally, the paper discusses a proposal for improvement in some aspects of the process: the business processes model management as software assets for their reuse, the automation of requirements elicitation based on such process assets, and the code generation of such requirements implementation.}

\onecolumn \maketitle \normalsize \vfill

%% Introduction
%% ============

\section{\uppercase{Introduction}}
\label{sec:introduction}

(Context)

(Problems detected).

(Proposal)

(Overview of the paper) Rest of this paper is organized as follows: Section \ref{sec:context} we describe the context which the experience was developed, Section \ref{sec:background} presents an overview of RUP, the modelling of requirements in RUP, and the modelling of business processes using BPMN. Section \ref{sec:challenges} describes our experience on several software development projects, the challenges we faced and the actions taken in the Software Process (SWP) realization. Section \ref{sec:proposal} presents the changed software process, Section \ref{sec:evaluation} presents effectiveness and efficiency data leading to the assessment of our changed SWP, explains the consequences of the results and draw conclusions.  
Finally, Section \ref{sec:conclusions} concludes the paper. 

%% Context
%% =======

\section{\uppercase{Context: Universidad de los Llanos}}
\label{sec:context}

In this paper, we are describing our experience on multiple academic and ``real-life" software development projects in the Universidad de los Llanos.

The \textit{Universidad de los Llanos (Unillanos)} is the biggest public university in the eastern plains in Colombia. 
% another sentence describing the U
Its Engineering Faculty offers multiple academic programs related to computer science and software engineering.
For instance, there is an undergraduate program in Systems Engineering and a postgraduate one in Software Engineering. 

\paragraph{Software Development in Academic projects:}
Students of the Systems Engineering and Software Engineering programs must develop software as part of some of their courses.
Courses such as \textit{Software Engineering foundations}, \textit{Software Project Management} and \textit{Software Modeling} imply a final project where students must create a software product. Usually, each one of these projects is performed by a team of five students. In average, there are five to six group-projects each semester per each course. 

The software to develop as final project is typically defined by the students themselves. 
For instance, in the last years we can mention projects in a rich variety of domains: one for the management of the work delegated to the lawyers in a buffet, other for electronic voting at Unillanos, the management of the musicians of Habana Productions, the management of university buses, a social network to share books, the management of laboratories and academic facilities, a platform for academic support provided by students, the management of the urban transport route model, an enrollment system for the language institute and another one for managing the clinical laboratory of the University.       


\paragraph{Software Development for ``real-life" projects:} \label{real-life}
In addition to the academic projects the Engineering Faculty has been involved in multiple development projects for ``real-life" applications since 2011.
At that time, the processes of the University was supported by a legacy application.
The CiENTiC academic unit was created to support the development of new solutions for the University with the participation of professors, faculty members and hired professional software engineers.

Among the most relevant real-life projects, we can mention: \textbf{ConD}, a software for managing the merit contest required in the Unillanos to be professor; \textbf{GAVi}, a generator for Moodle virtual classroom; 
\textbf{Huellas}, a system to manage the curriculum of professors and faculty members; 
\textbf{SARA}, a software for asigning academic responsibilities; 
\textbf{EVa}, a system for evaluating professors;
\textbf{Madiba}, a software to support conciliation as a legal instrument for conflict resolution in Colombia; \textbf{CarSharing}, a platform to share transport from and to Unillanos and the Villavicencio city;
\textbf{Bioflocos and SenVara}, platforms to analyze physical and environmental conditions obtained by Wireless sensor networks (WSN) for aquaculture and agriculture; and
\textbf{MAc}, a system to manage International Mobility of Academics. 
   

%% Background
%% ==========

\section{\uppercase{Software Development Processes}}
\label{sec:background}

Both types of software development projects (\ie academic and real-life projects) have been highly influenced by the methods and modeling notations presented in the academic courses in Unillanos.
Since 2010, the software engineering courses have been based on the Rational Unified Process (RUP).
The courses for organizational systems have been based on the Business Process Modeling and Notation (BPMN).
This section presents an overview of both. 

%% RUP
%% ===

\subsection{Rational Unified Process}

The \textit{Rational Unified Process (RUP)} is a customizable software development process framework based on the Unified Process (UP) and the Unified Modeling Language (UML) proposed at mid of the nineties~\cite{Jacobson:1999}.
The RUP and other variations such as OpenUP, EnterpriseUP and EssentialUP, have been widely adopted by companies around the world since their introduction.

RUP combines two perspectives: a \textit{technical perspective} that considers the different disciplines (workflows) that must be accomplished to develop the software, and an \textit{administrative perspective} that defines a set of project phases and iterations aimed to produce small improvements to the general product incrementally~\cite{Kruchten:1996}. 

\begin{figure}[ht!]
    \centering
    \includegraphics[width=0.45\textwidth]{images/RUP}
    \caption{RUP process, adapted from \cite{IBM-BM}}
    \label{fig:rup}
\end{figure}

Figure \ref{fig:rup} shows an overview of the  workflows and phases in RUP. 
The RUP is often presented as a matrix, where project phases of inception, elaboration, construction, and deployment are pitted against the technical disciplines of business modelling, requirements, analysis and design, implementation, and test and deployment. Originally, it included other three supporting engineering disciplines of configuration management, project management and environment management. Later, an additional it was extended to include the supporting discipline of production maintenance


%% Business Modeling
%% =================

\subsection{Business Modelling in RUP}

The RUP artifacts vary for each discipline activity and development phase.
For instance, according to the Schach's guidelines~\cite{Schach:2010}, the two artifacts constructed in the discipline for requirements are the glossary and the Use Cases, which are a set of documented use case diagrams with a brief description of what they allow to do and one step-by-step description of how it is done; in the next discipline, Analysis and Design the main artifacts are:  The Use Case Scenarios (describing situations when the use cases have success and other alternatives), user interface mock-ups, the entity class diagrams, machine state models, collaboration models (to discover limit and control classes), communication, sequence and robustness diagrams.


In RUP, the \textit{business modeling discipline} comprises activities for understanding the business processes by  identifying the business workers, their responsibilities and the tasks they perform.
These tasks are supported by two types of artifacts: 
The Business Use-Case Model (BUCM) used to represent business processes at strategic and tactical levels, and the Activity Diagrams (AD) used to represent operational workflows~\cite{IBM-BM}.

\subsubsection{Business Use-Case Model (BUCM)}

\begin{figure}[ht!]
    \centering
    \includegraphics[width=0.4\textwidth]{images/bucm4}
    \caption{Example BUCM, taken from \cite{IBM-BM}}
    \label{fig:example-bucm}
\end{figure}

\subsubsection{Activity Diagrams (AD)}

\ldots


%% Business Process
%% ================

\subsection{Modelling Business Processes}

In contrast to RUP, mainly focused on software requirements and designs, other approaches focus on modelling business processes.

The \textit{Business Process Modelling Notation (BPMN)} is aimed to ``bridge the gap between process design and implementation” \cite{BPMN}. It provides a standard notation that can be understood by Business Owners who manage and monitor the processes, Business Analysts who specify and design the processes, and Business Developers who implements the process in software systems.

In BPMN, a \textit{Business Process Diagram (BPD)} is a specialized flow-charting technique used to create graphical models of the business processes.

(example)

There are many specialized tools for creating and analysing BPD models. An analyst can use these tools, for instance, to detect errors in a diagram, to detect conflicts among diagrams and to simulate the behaviour of the process represented there.

\ldots xxxx

Organizations need information systems to support their core business. And this core business is represented in the different business processes that constitute the so-called operating and / or transactional business systems. However, companies must respond quickly to market conditions, which impact their business processes, as these must be adapted to change rules, redesign or sometimes, create others. Therefore, the software applications that support them must adapt at the same time with these changes, which does not always happen. It has been verified that there is a misalignment between the software applications and the business processes of the organization \cite{Prz14}. This inconsistency is mainly due to the lack of methodology and rigor to raise the requirements. On the other hand, several studies \cite{SL14},\cite{MAA12} have shown that raising requirements by modeling business processes is a good technique. Therefore, we are facing a  vicious circle, where the specification of requirements for software applications that support business processes must be done, precisely, with a business process-oriented approach.

IBM has implemented the integration of its BPMN Diagrams with UML UC mapping tasks (the more simple action in the process) in the process with use cases, with the aim to provide traceability and navigation from BP to UC and backwards. \cite{IBM-BM-UC-Doc} \cite{IBM-BM-UC-Release}; however, this plain mapping let out issues such orchestration, .... (what else?) 

%% Challenges 
%% ========== 

\section{\uppercase{The challenges and the changes on the SWP}}
\label{sec:challenges}

In the software development projects for the courses and for the institution, we faced several challenges and reacted to them mainly changing the Software development process to one that mixed some elements of RUP with the BPMN approach. This section presents the challenges and the changes proposed and implemented on the SWP. In the next section \ref {sec:proposal} we depict all software process resulted from the changes 

\subsection{Client domain}
What are the things the business do, how they are done, and where and how they can be improved with software.

\subsubsection{The things} Schach proposes to construct a glossary previously to the RUP business model \cite{Schach:2010} to acquire familiarity with the client domain and to use correct terminology when communicating with the client and potential users of the software. Here we found two challenges: Achieve well written definitions and that these were tested by the client. 
\textbf{Change:}After face several difficulties with the plain texts, both  with the developers (for writing) and the client (for testing), we introduced the concept map to illustrate in a graphic and synthetic way, the concepts and their relations. The figure \ref{fig:example-concept-map} shows a concept map example. 

\begin{figure}[ht!]
    \centering
    \includegraphics[width=0.4\textwidth]{images/Conceptual-Map}
    \caption{Example conceptual map.}
    \label{fig:example-concept-map}
\end{figure}

This concept map had to be constructed following basic rules that made it simple, clear and forceful in the expression of relationships. For example, each box denotes a concept and must be written with a noun or a noun phrase with a maximum of three words. The same maximum words must be used to the links that denotes the relations. The concept map was used for the developers team to verify the sense of their writing before going to the client and with him how a tool to  test the glossary. When some concept or relation showed on the map was not clear then its definition was consulted directly in the text on the glossary. In this way, the testing task of the client was changed from a reading task to a concept map feedback activity. Some data reflecting results of this change are showed on \ref{sec:evaluation}  

\subsubsection{How the things are done.} The business model proposed by RUP to describe the business processes of an organization, uses the UC and the AD diagrams; we faced several difficulties, both, from the client side and the developer team side. We had mistrust of the level of customer dominance over the artifacts of the business model and consequently about the certainty of the feedback received from them. Usually, we found inconsistencies between the client's approval of the artifacts and their verbal expressions about how their processes should function with the software. UC and AD are artifacts that are not mastered by the client and the documentation can be very extensive and prone to errors (mainly because it makes it difficult to carry out a quick, practical and safe review). On the developers' side, the business model, by reflecting the functionality of the new system, made it impossible to be sure that the team understood how things are currently done, which in turn made it difficult to establish the impact of the new system on those processes or the possible scope in developments that allowed gradual changes in favor of the organization, or even and most important, be sure the system you propose transforms the process in the way the business needs.( SARA es un claro ejemplo de "desconexión" entre la funcionalidad de un sistema y la forma en que se hacen las cosas en la organización.).
We have added a business process modeling on BPMN to draw how things are done in the client domain currently. We call it the BPMN AS-IS artifact. The effect this change caused is showed in \ref{sec:evaluation}. 

\subsubsection{how they can be improved with software.}  
Use Cases express the way the software must function, but the client understand the things their business do, that are not the same issue. As in understanding the client domain, get feedback of the client over the UC and AD artifacts is a task that showed us inconsistencies, mainly because there is not certainty about the domain that the client has of them. Apart from this, with the business model we take the client to a workspace that may not be familiar to him, which is the functionality of a software, where it may not be obvious the relationship of such functionality to the way things are done in the organization. We used a business process model on BPMN to draw how things should be done in the client domain with a software support. We call it the BPMN TO-BE artifact. Once this BPMN is approved by the client, based on him, we construct the business model, which becomes an artifact of internal use of the development team. 

\subsection{Requirement implementation scope definition (in a high level specification).} 
In RUP, requirements specification is made with use case diagram and the use case documentation. The first is composed mainly by icons, ovals and lines, that represent actors, system actions and association types, respectively. The lines, particularly, may represent inheritance, inclusion and extension; the second is the step-by-step use case description and the scenarios that could be presented in the use case execution. As stated previously, this kind of artifact is not mastered by the client and therefore is very difficult to deal some details with the client using it. 
Even more, if the client dominates the artifact, where (in the diagram or the documentation) can the scope of the implementation of a requirement be established? How do I establish it at a "high level" with the client? For example, if you talk about "send a communication", where o how, can we establish the implementation scope, which for this case can range from generating a pdf that should be printed and then sent by physical mail, to an automatic message by whatsapp (or some other message service) or by email? We used the BPMN TO-BE model to define scope using gates whose out flows represents the different alternatives. We represent disjunctive scopes with an exclusive gate and coexisting scopes with a parallel gate.   

\subsection{Roles.} From the Use Cases, the actors will derive in roles, but, to make a complete and correct role definition, it is necessary to appreciate the different activities where the actor is involved. In RUP, you need to use several artifacts, that begin with use cases and go trough several diagrams, describing several characteristics of the system and its functionality, to recognize where and how such role must be done. In itself this task, by the number and size of the diagrams, can become difficult, but what are about prove those definitions with the client? The client knows what do anyone in their business, but it is different the knowledge that must be accurately specified in terms of the role on the new software. It may be that someone can write some kind of textual definition that could be approved by the client, with all their implications. We used the BPMN TO-BE to define roles, which are the pools between the lanes on the process. Whenever be necessary pools are defined and their participation in the process is stayed, on manual, automatic or combined activities. The client can see, in the drawing of how his organization would work, who would do what. Again, as previously has been showed, the client showed us his understanding participating in the pools localization and definition. The client, on the business process model, realizes the lack of a participant or that some, initially modeled independently, play the same role. So, the actors of the Use Case Diagrams come from the lanes/pools of the BPMN.              

\subsection{Phases and actor interconnections.} when we found business processes with phases and actor interconnections between them, we faced a communication difficulty with the artifacts to involve the client in the feedback of such issues. RUP proposes the "work flow" diagram and "swim lanes" in it to specify a use case model business \cite{IBM-BM}, but it is complex, especially on the client side, see how organized are the involved elements (actors, human and software supported actions, intercommunication with messages or triggers, intermediate products, etc.) when there are phases. Even without phases, in RUP it is necessary to combine several artifacts to fully describe the business model. These artifacts, with the client as interlocutor, become complex. Similar situation happens when become necessary to show how messages flow between actors, in different phases or generating in some cases, new activities (human or software supported), inside or outside the system limits (an external qualification credit or payment system, i.e.). We use the BPMN Milestones to describe phases in the process and the client can see and feedback their limits and the flows between each one to define the messages and interconnections necessary for a correct process execution.      

\subsection{Requirements changes management.} Like the domino effect, a requirement change implies changes in the artifacts used to define such requirement implementation, and this is a permanent, characteristic and predictable event. This was the most difficult task to carry out when developing software in the earlier time at Unillanos. Invariably, the result was a set of outdated and disintegrated artifacts. The situation worsens when the change occurs after several iterations, when you already have models of state machines, collaboration, communication and sequence diagrams. We faced several difficulties in effort, time and coherence to maintain all the set of artifacts updated. We react to that with several actions. We preserved the BPMN TO-BE updated, and consequently the associated use case, but from then on, we made some changes. 

\subsubsection{test cases.} First of all, we changed our text documents of step-by-step description and scenarios by sheets that show the involved data before and after the Use Cases realizations. The first Use Case realization is the success-defined scenario and match (and replace) the text document step-by-step description. The rest of scenarios are transformed in the same way. Each sheet shows the data transformation resulted from the use case execution and the message, if any, the system must sent. This artifact allowed a great change in the test of scenarios by the client, changing from reading several plain texts to check the changes in the data and the messages that must be sent. Yet the task is not easy neither fast, but we reach a more precise definition of what must happen with a Use Case realization. In addition to facilitating the review work, this change allowed adjustments, originated by changes in requirements, to be more rapid. We called this sheets the test cases.

\subsubsection{Obsolete artifacts.} Secondly, we declared the state machine, communication, and sequence diagrams obsolete and did not more maintenance on them. After the adjustment on test cases, we verified possible changes on the collaboration diagram and then we passed to the robustness diagram to complete the effect on analysis and design produced by the requirement change. At this point we must enforce the useful that the BPMN AS-IS and TO-BE are in requirement changes. The client and the development team uses the BPMN to clarify doubts that arise permanently in the process and this task is did in a very quick way. 

\subsection{Team or client members changes.} In all projects that we lead, we had changes in the development team or in the client or in both. To count on the new artifacts proposed and built in the process, was the lifeguard or the parachute that allowed us to overcome the difficult situation that these changes represented. These artifacts allowed to be involved with the client domain and with the changes in it by the software in construction, in a very quickly way. The member changes caused impact in the way the process was working, but we are aware that the understanding of the domain and the project scope and its effects were minimized.     

Like the domino effect, a requirement change implies changes in the artifacts used to define such requirement implementation, and this is a permanent, characteristic and predictable event. This was the most difficult task to carry out when developing software in the earlier time at Unillanos. Invariably, the result was a set of outdated and disintegrated artifacts. The situation worsens when the change occurs after several iterations, when you already have models of state machines, collaboration, communication and sequence diagrams. We faced several difficulties in effort, time and coherence to maintain all the set of artifacts updated. We react to that with several actions. We preserved the BPMN TO-BE updated, and consequently the associated use case, but from then on, we made some changes. 

\subsection{Communication with the client} In short, one of the biggest difficulties we faced was communication with the client. And our changes were answers to that, mainly because we were aware that this is a critical success or failure factor in a software development project[CHAOS]. In the same way, we were aware that natural language was not the solution (it should be worst) (ref de "balas de plata" o alguno de los autores que habla de la dificultad en tener certeza que lo que dijimos fue lo que se entendió) and using graphics artifacts, with simple sintax as conceptual maps and BPMNs, and changing textual behaviours documents by data transformation sheets  were bridges that reduced the gap in understanding to all. We have two examples of this issue in the Madiba an EVA projects, both with business domains widely supported by extensive legal documents but with minimal structures of synthesis or representation. Once each new proposed artifact of this process was constructed, it was the first reference source to solve doubts or revise something. Besides ameliorate our process, we contributed to the client in his knowledge management.  

%% Proposal 
%% ========== 

\section{\uppercase{Software Process Proposal}}
\label{sec:proposal}

The process resulted from the changes is depicted on figure  \ref{fig:changed-swp}. Next we list and describe the changes incorporated in the process. 


\begin{figure*}[ht!]
    \centering
    \includegraphics[width= 0.95 \textwidth]{images/Software-Process}
    \caption{Changed Software Process.}
    \label{fig:changed-swp}
\end{figure*}
    
\subsection{Domain Model in Requirements phase}

The subprocesos "Model domain" in the requeriments phase is compossed of: 
\begin{itemize}[noitemsep,nosep]
    \item {Glossary}
    \item {Conceptual Map}
    \item {Business Process Model on BPMN, version "AS-IS"}
\end{itemize}

\subsubsection{Conceptual map.}

\subsubsection{Business process Model on BPMN, version "TO-BE" previous to Use Cases.}

\subsection{Domain Model in Requirements phase}

%% Evaluation 
%% ============

\section{\uppercase{Evaluation}}
\label{sec:evaluation}


\subsection{Consistency among theory and development???}

Our experience on software development, according to section \ref{sec:challenges}, has been enriching, since it has allowed us to compare the methodological theory imparted in the courses with the practice of software development. The implementation of the projects mentioned in \ref{real-life} led us to prove, in a qualitative way, that the methodology alone does not guarantee the expected results when it comes to the development of non-academic products. 

The experience obtained with the development of these products, allows us to do a qualitative evaluation of the used methodology. In a way, using the idea of triangulation~\cite {Mye09} has allowed to obtain better results that if only we had followed, in our case, the methodology RUP. 

Our experience with the development of software projects has allowed us to conclude that it is necessary to complement the methodological theory with other elements taken from experience of the developer team. In addition, it is also necessary to take into account the context of the problem, in order to optimize the new elements that will accompany the methodological theory. In this case, using the business processes in the different stages of development allowed to improve the communication between the client and the developer team, to determine the scope of the requirements to a level of abstraction adapted, to improve the development times and to react more efficiently to changes in processes when necessary.

The main / first? difficulty that could be overcome was to achieve that the client understood the concepts of the domain of their business and their relationships with each other. The glossary~\cite{Schach:2010} together with the conceptual maps made it possible to establish clearly the business' domain in a shorter time for both, the client and the developer team.


%\subsection{Consistency among Processes and Use cases}


%As pointed by other authors \cite{Wautelet:2017}, the models used to model business processes are often not related one to the others. Three is not an explicit traceability among the elements in the BUCM and the AD. This may result in inconsistencies among the high-level processes and the lower-level workflows. There are several proposals aimed to overcome limitations of the RUP Business Modelling by using BPMN Business Process Diagrams (BPDs) and explicit traceability among BUCMs and BPDs.

%% Conclusion and future work
%% ===========

\section{\uppercase{Conclusions}}
\label{sec:conclusions}

\subsection{Future work}

\subsubsection{business process and implementation reuse} Currently we are facing the challenge of developing software to similar client domains, with similar business processes. Two cases that illustrate it are at Unillanos and Conalbos. First one between the Editorial and the DGI (by their spanish innitials "Dirección General de Investigaciones) with their processes of contest to publish and to obtain research resources, respectively; the second one between the conciliation and the natural person insolvency judicial procedements. The business processes in the Editorial and DGI in general terms are the same: There is an announcement, some requirements to participate, a deadline, a peer evaluation, a final result and an adjudgment (admission of the publication or delivery of the resources). In the same way, the process that must be realized to do a conciliation or an insolvency for natural person is similar: there is a request, there are required (or summoned) entities (natural o legal), there is an assignment of judge (conciliator or adviser, per turn or chosen by the applicant), setting the date of hearing, conducting the hearing and the generation of the result thereof.

\subsection{Code Generation based on Business Process Models}

(other). 

\section*{\uppercase{Acknowledgements}}

\noindent If any, should be placed before the references section
without numbering.

%% Bibliography
%% ============

\bibliographystyle{apalike}     %% APA
{\small
\bibliography{bibliography/bibliography}}

\end{document}

