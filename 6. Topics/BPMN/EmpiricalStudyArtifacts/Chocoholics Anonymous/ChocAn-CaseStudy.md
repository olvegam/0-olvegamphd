#   Chocohólicos Anónimos

Chocohólicos Anónimos (ChocAn) es una organización dedicada a ayudar a las personas adictas al chocolate en todas sus gloriosas formas. Los miembros, por el pago de una cuota mensual, tienen acceso a consultas ilimitadas y tratamientos con profesionales de la salud, principalmente nutricionistas, médicos internistas y entrenadores. La organización planea sistematizar toda su operación para funcionar como se describe a continuación:  



## ChocAn Sistematizado 

La membresía otorga un carné con nómbre y número de identificación en relieve y una banda magnética con esa información.  Cada profesional de la salud que es proveedor de servicios para ChocAn tendrá en su consultorio un dispositivo lector de estas tarjetas.  

Para recibir un servicio de salud, el miembro entrega su tarjeta al proveedor, éste la pasa por el dispositivo, que se comunica con el data center de ChocAn y muestra en pantalla el resultado de la validación del miembro. Las posibles respuestas son "Válido" o "No válido" o "Suspendido". El último mensaje indica que el miembro tiene por lo menos una cuota vencida.   

Un proveedor factura un servicio a ChocAn una vez lo haya prestado, para ello pasa nuevamente la tarjeta del miembro por el dispositivo lector o digita el número de identificación del miembro. Una vez que aparece la palabra "Válido"el proveedor digita la fecha de prestación del servicio (esto permite manejar eventos en los que el servicio no se haya podido facturar en el momento inmediatamente despues de prestarlo), luego digita el código del servicio que acaba de prestar ( lo puede consultar en un listado que tiene a la mano) y el dispositivo despliega el nombre del servicio. Si es correcto, puede agregar comentarios sobre el servicio prestado y a continuación verá desplegado el valor que le será pagado por el servicio. 

Si todo está bien, el dispositivo envía al data center un registro con la siguiente información:
*  Fecha y hora actuales
*  Fecha de prestación del servicio
*  Código del proveedor
*  Identificación del miembro
*  Código del servicio
*  Comentarios (opcional)

Un proveedor puede solicitar en cualquier momento al centro de datos de ChocAn un listado de códigos de servicio y éste le es enviado por correo electrónico.   

Todos los viernes a media noche, en el centro de datos de ChocAn se ejecuta un cierre contable que lee todos los registros de servicios facturados y: 

1. Genera los reportes requeridos y los envía a quien corresponda. Durante la semana la gerencia de ChocAn puede ejecutar individualmente cualquiera de estos reportes. 
2. Genera los archivos de transferencia electrónica de fondos (TEF) y los envía a las entidades financieras correspondientes.
3. Los miembros que han hecho uso de los servicios de ChocAn durante la semana reciben por correo electrónico un reporte de los servicios que utilizaron, ordenados por fecha de prestación del servicio. 
4. De la misma forma, los proveedores que han facturado servicios a ChocAn durante la semana reciben un reporte con los servicios que han prestado, con el total de servicios prestados y el valor total que le será pagado.  
5. Finalmente, genera un reporte resumen con la lista de proveedores que recibirán pago, el número total de consultas que realizó y el valor total que le será abonado. En el reporte se totalizan los proveedores, los servicios prestados y el valor pagado.

En el centro de datos de ChocAn también hay un software para gestionar los miembros y los proveedores (agregar, retirar y actualizar).

-------
Los procesos financieros externos de gestión de pago de cuotas de miembros han sido contratados a un tercero, quien se encargará además de la suspensión a miembros cuya cuota ha vencido y la reactivación a quienes se pongan al día. El software del tercero realizará las actualizaciones de los registros relevantes en el centro de datos de ChocAn, todos las noches a las 9:00pm. 

Las comunicaciones y el lector de tarjetas también han sido contratados a un tercero.  

A usted le ha sido otorgado el contrato para el desarrollo del software del centro de datos. 








 
